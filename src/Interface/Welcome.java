package Interface;

import InteractionsGame.StartGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Welcome extends Scene implements EventHandler<ActionEvent>{

	//BUTTON CONTAINER INITIALIZER
	private AnchorPane background;
	private Instructions sceneInstructions;
	private StartGame sceneGame;
	private Stage mainStage;
	private Options sceneOption;
	private Credits sceneCredits;
	private Classification sceneClassification;
	private Archivador sceneArchive;
	private MusicPlayer music;
	
	//INITIALIZER OF THE OBJECTS
	private Button classification;
	private Button credits;
	private Button instructions;
	private Button exitGame;
	private Button board;
	private Button options;
	
	//INITIALIZER OF CONTAINERS AND EXTRAS
	private ImageView backgroundImage;
	private VBox buttons;
	
	
	/**
	 * CONSTRUCTOR OF THE MAIN SCREEN OF THE GAME
	 * 
	 * @param ROOT DATA TYPE GROUP
	 * @param WIDTH GIVES THE SIZE OF THE SCREEN WIDTH
	 * @param HEIGHT GIVES THE SCREEN HIGH SIZE
	 */
	public Welcome(Group root, double width, double height,Stage mainStage) {
		super(root);
		
		//CREATE THE ANCHOR PANE WHERE ALL THE DATA WILL BE PLACED
		background = new AnchorPane();
		background.setMinSize(width, height);
		this.mainStage=mainStage;
		
		
		
		//ASSIGN AND CREATE BUTTONS AND STYLES TO THE BUTTONS, IN ADDITION TO CREATING THE BOX AND SAVING THE BUTTONS
		instructions=new Button("INSTRUCCIONES");
		board=new Button("INICIAR JUEGO");
		options=new Button("OPCIONES");
		exitGame=new Button("SALIR DEL JUEGO");
		credits= new Button("CREDITOS");
		classification=new Button("CLASIFICACION");
		
		
		//ASSIGNMENT OF THE SHARES
		instructions.setOnAction(this);
		options.setOnAction(this);
		board.setOnAction(this);
		exitGame.setOnAction(this);
		credits.setOnAction(this);
		classification.setOnAction(this);
		
		//SIZE AND COLOR EDITION
		credits.setMaxWidth(Double.MAX_VALUE);
		classification.setMaxWidth(Double.MAX_VALUE);
		instructions.setMaxWidth(Double.MAX_VALUE);
		board.setMaxWidth(Double.MAX_VALUE);
		options.setMaxWidth(Double.MAX_VALUE);
		exitGame.setMaxWidth(Double.MAX_VALUE);
		credits.setStyle("-fx-base: rgb(255,120,44);");
		classification.setStyle("-fx-base: rgb(255,120,44);");
		instructions.setStyle("-fx-base: rgb(255,120,44);");
		board.setStyle("-fx-base: rgb(255,120,44);");
		options.setStyle("-fx-base: rgb(255,120,44);");
		exitGame.setStyle("-fx-base: rgb(255,120,44);");
		
		//ASSIGNMENT OF BOXES AND CONTENTS
		buttons = new VBox();
		buttons.getChildren().addAll(board,instructions,classification,options,credits,exitGame);
		
		//ASSIGN THE BACKGROUND IMAGE THAT WILL HAVE THE SCREEN
		backgroundImage = new ImageView ("Images/infierno.jpg");
		backgroundImage.setFitHeight(height);
		backgroundImage.setFitWidth(width);
		
		//POSITION THE SET OF BUTTONS IN THE CENTER OF THE PANEL
		AnchorPane.setTopAnchor(buttons, 250.0);
		AnchorPane.setLeftAnchor(buttons, 450.0);
		background.getChildren().addAll(buttons);
		
		//AGREGA LAS COSAS A ALA ESCENA Y AL ANCHOR PANE
		//windowSelector = 0;
		root.getChildren().addAll(backgroundImage,background);
		music=new MusicPlayer();
		music.playMenu();
		sceneArchive=new Archivador();
		sceneClassification=new Classification(new AnchorPane(), 1000, 600, mainStage, this, sceneArchive);
		sceneOption =new Options(new Group(),  1000, 600,mainStage,this,music);
		sceneInstructions=new Instructions(new AnchorPane(),  1000, 600,mainStage,this);
		sceneCredits=new Credits(new Group(), mainStage,this);
		
	}

	/**
	 * SHOW THE INSTRUCTIONS
	 * @return the instructions
	 */
	public Button getInstructions() {
		return instructions;
	}

	/**
	 * SHOW THE BOARD
	 * @return the board
	 */
	public Button getBoard() {
		return board;
	}

	/**
	 * SHOW THE OPTIONS
	 * @return the options
	 */
	public Button getOptions() {
		return options;
	}

	/**
	 * SHOW THE EXITGAME
	 * @return the exitGame
	 */
	public Button getExitGame() {
		return exitGame;
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(getBoard())) {
			sceneGame = new StartGame(new Group(), 750, 850,sceneOption,mainStage,this,sceneArchive,music,sceneClassification);
			mainStage.setScene(sceneGame);
			if(sceneGame!=null) {
				sceneGame.runLevel();
				sceneGame.runCharacter();
				sceneGame.runMusic();
			}
		}else if(event.getSource().equals(getInstructions())) {
			mainStage.setScene(sceneInstructions);
		}else if(event.getSource().equals(getOptions())) {
			mainStage.setScene(sceneOption);
		}else if(event.getSource().equals(getExitGame())) {
			System.exit(0);
		}else if(event.getSource().equals(credits)) {
			mainStage.setScene(sceneCredits);
		}else if(event.getSource().equals(classification)) {
			mainStage.setScene(sceneClassification);
		} 
	}
	
}
