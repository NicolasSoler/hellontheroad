package Interface;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Instructions extends Scene implements EventHandler<ActionEvent>{

	private AnchorPane one0;
	private AnchorPane two0;
	private AnchorPane copi0;
	
	//INITIALIZATION OF THE OBJECTS
	private ImageView keys;
	private Stage mainStage;
	private Welcome sceneWelcome;
	private Button returnW;
	private Button anterior;
	private Button siguiente;
	
	//CREATE A BOX AND TAKE ALL THE KEYS THERE
	private VBox partOne;
	private HBox partW;
	private Label label2;
	private ImageView KeyW;
	
	private HBox partA;
	private Label label3;
	private ImageView keyA;
	
	private HBox partD;
	private Label label4;
	private ImageView keyD;
	
	private HBox partS;
	private Label label6;
	private ImageView keyS;
	
	private Label Label1;
	private VBox playability;
	
	private Label titulo;
	private VBox play;
	
	
	private VBox partOne2;
	private HBox partPrincipal;
	private Label labelPrincipal;
	
	
	private HBox cajaAutoPrincipal;
	private Label labelAutoPrincipal;
	private ImageView autoPrincipal;
	
	private HBox cajaEnemigos;
	private Label labelEnemigos;
	private ImageView Enemigos;
	
	
	private HBox cajaEnemigo;
	private Label labelEnemigo;
	private ImageView Enemigo;
	
	private Label Bonus;
	private VBox playBonus;
	
	private VBox partTwo;
	private HBox partSecond;
	private Label labelSecond;
	
	private HBox cajaNavePrincipal;
	private Label labelNavePrincipal;
	private ImageView navePrincipal;
	
	private HBox cajaMoney;
	private Label labelMoney;
	private ImageView Money;
	
	private HBox total3;
	private HBox total2;
	
	//CREATE ANOTHER BOX AND MAKE ALL THE CHARACTERISTICS THERE
	private VBox parteDos;
	private Label fury;
	private Label objective;
	
	//ORGANIZE BOTH BOXES IN ONE
	private HBox total;
	private int pos;
	
	public  Instructions(AnchorPane root, double width, double height,Stage mainStage, Welcome sceneWelcome) {
		super(root);
		copi0=root;
		pos=0;
		root.setMinSize(width, height);
		one0=new AnchorPane();
		one0.setMinSize(width, height);
		two0=new AnchorPane();
		two0.setMinSize(2000, 2000); 
		this.mainStage=mainStage;
		this.sceneWelcome=sceneWelcome;
		keys = new ImageView("Images/todasteclas.png");
		returnW=new Button("VOLVER AL MENU");
		returnW.setMaxWidth(200);
		returnW.setStyle("-fx-background-color: rgb(108,21,0);");
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		returnW.setOnAction(this);
		
		anterior=new Button("ANTERIOR");
		anterior.setMaxWidth(100);
		anterior.setStyle("-fx-background-color: rgb(108,21,0);");
		anterior.setStyle("-fx-base: rgb(153,0,23);");
		anterior.setOnAction(this);
		
		siguiente=new Button("SIGUIENTE");
		siguiente.setMaxWidth(100);
		siguiente.setStyle("-fx-background-color: rgb(108,21,0);");
		siguiente.setStyle("-fx-base: rgb(153,0,23);");
		siguiente.setOnAction(this);
		
		/**
		 * CHARACTERISTICS OF THE TITLE BOX
		 */
		Label1 = new Label("Jugabilidad:");
		playability = new VBox(Label1);
		playability.setStyle("-fx-base: rgb(153,0,23);");
		
		/**
		 * CHARACTERISTICS OF THE OTHER BOXES LIKE THE TEXT AND THE IMAGES
		 */
		
		label6 = new Label("-S: realiza un desplazamiento en el eje �Y� hacia abajo. \n");
		keyS = new ImageView("Images/teclaS.png") ;
		partS = new HBox(keyS, label6);
		partS.setSpacing(10);
		
		
		label2 = new Label("-W: realiza un desplazamiento en el eje �Y� hacia arriba. \n");
		KeyW = new ImageView("Images/teclaW.png");
		partW = new HBox(KeyW,label2);
		partW.setSpacing(10);
		
		label3 = new Label("-A: realiza un desplazamiento en el eje �X� hacia la izquierda.\n");
		keyA = new ImageView("Images/teclaA.png");
		partA = new HBox(keyA, label3 );
		partA.setSpacing(10);
		
		label4 = new Label("-D: realiza un desplazamiento en el eje �X� hacia la derecha. \n");
		keyD = new ImageView("Images/teclaD.png");
		partD = new HBox(keyD, label4 );
		partD.setSpacing(10);
		
		/**
		 * ASSIGN EDGES TO THE BOXES OF THE KEYS
		 */
		partOne = new VBox(partW, partA, partS, partD);
		VBox.setMargin(partW, new Insets(20,20,20,20));
		VBox.setMargin(partA, new Insets(20,20,20,20));
		VBox.setMargin(partS, new Insets(20,20,20,20));
		VBox.setMargin(partD, new Insets(20,20,20,20));
		
		partOne.setStyle("-fx-background-color: #330011");
		partOne.setStyle("-fx-base: rgb(153,0,23);");
		
		fury = new Label("Furia: La furia es tu sed de venganza, las ganas de conseguir la sangre de todos\n"
				+ " aquellos que te lastimaron, con el tiempo, esa furia disminuye y solo la consigues\n "
				+ "con objetos especiales que recuerdan tu pasado, o llegando al objetivo, ten cuidado,\n"
				+ " si te quedas sin furia pierdes." );
		
		
		objective = new Label("Objetivo: Llegar a la bandera, evitando todos los autos, aviones y destruyendo\n"
				+ " a los jefes hasta llegar donde el rey demonio y derrotarlo para quedarte con el trono\n "
				+ "y poder vengarte de todo lo que te hicieron\n en vida, y todo eso, sin que te quedes sin\n"
				+ " sed de venganza.");
		parteDos = new VBox(fury, objective,keys);
		
		VBox.setMargin(keys, new Insets(100, 100, 100, 100));
		VBox.setMargin(fury, new Insets(10, 10, 10, 10));
		VBox.setMargin(objective, new Insets(10, 10, 10, 10));
		parteDos.setStyle("-fx-background-color: rgb(95,21,0);");
		parteDos.setStyle("-fx-base: rgb(0,0,23);");
		total = new HBox(partOne, parteDos);
		total.setSpacing(50);
		/**
		 * ORGANIZE ALL BOXES AND THE FUND
		 */
		
		AnchorPane.setTopAnchor(playability, 10.0);
		AnchorPane.setLeftAnchor(playability, 10.0);
		AnchorPane.setTopAnchor(total, 30.0);
		AnchorPane.setLeftAnchor(total, 20.0);
		AnchorPane.setBottomAnchor(returnW, 10.0);
		AnchorPane.setRightAnchor(returnW, 10.0);
		AnchorPane.setBottomAnchor(siguiente, 10.0);
		AnchorPane.setRightAnchor(siguiente, 200.0);
		AnchorPane.setBottomAnchor(anterior, 10.0);
		AnchorPane.setRightAnchor(anterior, 300.0);
		
		one0.getChildren().addAll(playability,total);
        one0.setStyle("-fx-background-color: #330011");
        
        titulo = new Label("MODO CARRO");
		play = new VBox(titulo);
		play.setStyle("-fx-base: rgb(153,0,23);");
		
		
		labelPrincipal = new Label("Este es el principal modo en el que nuestro personaje es un auto el cual\r"
				+ "debe tratar de no perder furia y no chocar con los laterales para no perder.");
		
		partPrincipal = new HBox(labelPrincipal);
		partPrincipal.setSpacing(9);
		
		
		labelAutoPrincipal = new Label("\r"
				+ "Este es nuestro auto el cual podemos manejar a nuestro gusto (derecha,\r "
				+ "izquierda, arriba, abajo).\r");
		autoPrincipal = new ImageView("Images/autoPrincipal.png");
		cajaAutoPrincipal = new HBox(labelAutoPrincipal, autoPrincipal);
		cajaAutoPrincipal.setSpacing(9);
		
		
		labelEnemigos = new Label("\r"
				+ "\r"
				+ "Al chocar con estos carros enemigos har� que nuestro\r "
				+ "personaje pierda furia y tambien pierda el control.");
		Enemigos = new ImageView("Images/Enemigo.png"); 
		Enemigos.setFitHeight(100);
		Enemigos.setFitWidth(150);
		cajaEnemigos = new HBox(labelEnemigos, Enemigos);
		cajaEnemigos.setSpacing(9);
		
		
		labelEnemigo = new Label("\r"
				+ "\r"
				+ "Al chocar con este auto har� que se destruya nuestro auto y perdamos\r "
				+ "inmediatamente.");
		Enemigo = new ImageView("Images/Enemigos.png");
		cajaEnemigo = new HBox(labelEnemigo, Enemigo);
		cajaEnemigo.setSpacing(9);
		

		partOne2 = new VBox(partPrincipal, cajaAutoPrincipal, cajaEnemigos, cajaEnemigo);
		VBox.setMargin(partPrincipal, new Insets(20, 20, 20, 20));
		VBox.setMargin(cajaAutoPrincipal, new Insets(20, 20, 20, 20));
		VBox.setMargin(cajaEnemigos, new Insets(20, 20, 20, 20));
		VBox.setMargin(cajaEnemigo, new Insets(20, 20, 20, 20));
		
		partOne2.setStyle("-fx-background-color: #330011");
		partOne2.setStyle("-fx-base: rgb(153,0,23);");
		
		total2 = new HBox(partOne2);
		total2.setSpacing(40);
		
		
		Bonus = new Label("BONUS");
		playBonus = new VBox(Bonus);
		playBonus.setStyle("-fx-base: rgb(153,0,23);");
		
		
		labelSecond = new Label("Este es el Bonus modo en el que nuestro personaje es una nave la cual\r"
				+ "debe tratar de coger todas la monedas posibles en un tiempo limitado.");
		
		partSecond = new HBox(labelSecond);
		partSecond.setSpacing(9);
		

		
		labelNavePrincipal = new Label("\r"
				+ "Este es nuestra nave la cual podemos manejar \n a nuestro gusto (derecha,\r "
				+ "izquierda, arriba, abajo).\r");
		navePrincipal = new ImageView("Images/Navepricipal.png");
		navePrincipal.setFitHeight(100);
		navePrincipal.setFitWidth(80);
		cajaNavePrincipal = new HBox(labelNavePrincipal, navePrincipal);
		cajaNavePrincipal.setSpacing(9);
		
		
		labelMoney = new Label("\r"
				+ "estos son los puntos de bonificacion, \n las monedas nos ayudan a aumentar la furia.\r ");
		Money = new ImageView("Images/monedas.png");
		Money.setFitHeight(80);
		Money.setFitWidth(80);
		
		cajaMoney = new HBox(labelMoney, Money);
		cajaMoney.setSpacing(9);
		
		partTwo = new VBox(partSecond, cajaNavePrincipal, cajaMoney);
		VBox.setMargin(partSecond, new Insets(20, 20, 20, 20));
		VBox.setMargin(cajaNavePrincipal, new Insets(20, 20, 20, 20));
		VBox.setMargin(cajaMoney, new Insets(20, 20, 20, 20));
		
		partTwo.setStyle("-fx-background-color: #330011");
		partTwo.setStyle("-fx-base: rgb(153,0,23);");
		
		total3 = new HBox(partTwo);
		total3.setSpacing(40);
		
		AnchorPane.setTopAnchor(play, 10.0);
		AnchorPane.setLeftAnchor(play, 10.0);
		AnchorPane.setTopAnchor(playBonus, 10.0);
		AnchorPane.setLeftAnchor(playBonus, 700.0);
		AnchorPane.setTopAnchor(total2, 30.0);
		AnchorPane.setLeftAnchor(total2, 30.0);
		AnchorPane.setTopAnchor(total3, 30.0);
		AnchorPane.setLeftAnchor(total3, 600.0);
		
		two0.getChildren().addAll(play, total3, playBonus,total2);
		two0.setStyle("-fx-background-color: #330011");
        copi0.getChildren().addAll(one0,returnW,siguiente,anterior);
	}
	/**
	 * @return the returnW
	 */
	public Button getReturnW() {
		return returnW;
	}
	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(getReturnW())) {
			mainStage.setScene(sceneWelcome);
		}else if(event.getSource().equals(siguiente)) {
			if(pos==0) {
				copi0.getChildren().clear();
				copi0.getChildren().addAll(two0,returnW,siguiente,anterior);
				pos=1;
			}else {
				pos=0;
				copi0.getChildren().clear();
				copi0.getChildren().addAll(one0,returnW,siguiente,anterior);
			}
		}else if(event.getSource().equals(anterior)) {
			if(pos==0) {
				pos=1;
				copi0.getChildren().clear();
				copi0.getChildren().addAll(two0,returnW,siguiente,anterior);
			}else {
				pos=0;
				copi0.getChildren().clear();
				copi0.getChildren().addAll(one0,returnW,siguiente,anterior);
			}
		}
	}
}
