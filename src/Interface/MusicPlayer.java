package Interface;

import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicPlayer {
	
	private Media musicMenu;
	private Media musicGame;
	private Media musicBonus;
	private Media musicEndGame;
	private Media musicExplosion;
	private MediaPlayer playeExplosion;
	private MediaPlayer playerGame;
	private MediaPlayer playerMenu;
	private MediaPlayer playerBonus;
	private MediaPlayer playerEndGame;
	private double volume;
	private GameEfects gameEfects;
	
	
	public MusicPlayer() {
		volume=0.5;
		gameEfects=new GameEfects();
		
		musicEndGame=new Media(new File("src/Images/ENDGAME.mp3").toURI().toString());
		musicMenu=new Media(new File("src/Images/MENU.mp3").toURI().toString());
		musicGame=new Media(new File("src/Images/GAME.mp3").toURI().toString());
		musicBonus=new Media(new File("src/Images/BONUS.mp3").toURI().toString());
		musicExplosion=new Media(new File("src/Images/EXPLOSION.mp3").toURI().toString());
		
		playeExplosion=new MediaPlayer(musicExplosion);
		playeExplosion.setVolume(volume);
		playerEndGame=new MediaPlayer(musicEndGame);
		playerEndGame.setVolume(volume);
		playerMenu=new MediaPlayer(musicMenu);
		playerMenu.setVolume(volume);
		playerGame=new MediaPlayer(musicGame);
		playerGame.setVolume(volume);
		playerBonus=new MediaPlayer(musicBonus);
		playerBonus.setVolume(volume);
		
	}
	
	
	
	/**
	 * SHOW THE GAME EFECTS
	 * @return the gameEfects
	 */
	public GameEfects getGameEfects() {
		return gameEfects;
	}
	
	/**
	 * REPRODUCE THE EXPLOSION
	 */
	public void playExplosion() {
		playeExplosion.play();
	}
	
	/**
	 * REPRODUCE THE MAIN MUSIC
	 */
	public void playMenu() {
		playerMenu.play();
	}
	
	/**
	 * REPRODUCE THE BONUS
	 */
	public void playBonus() {
		playerBonus.play();
	}
	
	/**
	 * REPRODUCE THE END MUSIC
	 */
	public void playEndGame() {
		playerEndGame.play();
	}
	
	/**
	 * REPRODUCE THE GAME MUSIC
	 */
	public void playGame() {
		playerGame.play();
	}
	
	/**
	 * PAUSE THE MAIN MUSIC
	 */
	public void pauseMenu() {
		playerMenu.stop();
	}
	
	/**
	 * PAUSE THE END MUSIC
	 */
	public void pauseEndGame() {
		playerEndGame.stop();
	}
	 /**
	  * PAUSE THE GAME MUSIC
	  */
	public void pauseGame() {
		playerGame.stop();
	}

	/**
	 * PAUSE THE BONUS GAME
	 */
	public void pauseBonus() {
		playerBonus.stop();
	}
	
	/**
	 * CHANGE THE VOLUME OF THE MUSIC
	 * @param volume
	 */
	public void changeVolume(double volume) {
		this.volume=volume;
	}
	
	/**
	 * UPDATE THE VOLUME 
	 * @param volume
	 */
	public void updateVolume(double volume) {
		playerEndGame.setVolume(volume);
		playerMenu.setVolume(volume);
		playeExplosion.setVolume(volume);
		playerGame.setVolume(volume);
		playerBonus.setVolume(volume);
	}
}
