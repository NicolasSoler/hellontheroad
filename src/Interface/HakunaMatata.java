
package Interface;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * CLASE PRINCIPAL DEL JUEGO
 * @author JOHAN SOLER
 * @author FREDDY PATARROYO
 * @author DANNY OCHOA
 * @author LAURA BARAJAS
 *
 */
public class HakunaMatata extends Application {
	
	private Welcome firstStep;
	
	@Override
	public void start(Stage mainStage) throws Exception {
		
		firstStep = new Welcome(new Group(), 1000, 600,mainStage);
		
		mainStage.setScene(firstStep);
		mainStage.setResizable(false);
		mainStage.setTitle("Hell On The Road");
		mainStage.getIcons().add(new Image("Images/infierno.jpg"));
		mainStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}