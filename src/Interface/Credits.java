package Interface;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Credits extends Scene implements EventHandler<ActionEvent>{

	//INITIALIZER OF OBJECTS
	private ImageView credits;
	private ScrollPane scrollPane = new ScrollPane();
	private Welcome sceneWelcome;
	private Stage mainStage;
	
	//INITIALIZER OF BUTTONS AND BOXES
	private VBox box;
	private VBox finale;
	private Button returnW;
	
	
	public Credits(Group root,Stage mainStage,Welcome sceneWelcome) {
		super(root,1000,600);
		credits=new ImageView(new Image ("Images/CREDITOS.png"));
		box=new VBox();

		this.mainStage=mainStage;
		this.sceneWelcome=sceneWelcome;
		box.getChildren().add(scrollPane);
		VBox.setVgrow(scrollPane, Priority.ALWAYS);
		
		returnW=new Button("VOLVER AL MENU");
		returnW.setMaxWidth(200);
		returnW.setStyle("-fx-background-color: rgb(108,21,0);");
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		returnW.setOnAction(this);
		
		credits.setFitWidth(980);
		credits.setPreserveRatio(true);
		finale=new VBox();
		
		finale.getChildren().add(credits);
		scrollPane.setVmax(140);
		scrollPane.setPrefSize(1000, 600);
		scrollPane.setPannable(true);
		scrollPane.setContent(finale);
		
		root.getChildren().addAll(box,returnW);
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(returnW)) {
			mainStage.setScene(sceneWelcome);
		}
		
	}
}
