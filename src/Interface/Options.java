package Interface;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Options extends Scene implements EventHandler<ActionEvent>{
	
	//INITIALIZATION OF THE BUTTONS
	private Button levelDifficultyeasy;
	private Button levelDifficultymiddle;
	private Button levelDifficultyhard;
	private Button accept;
	private Button returnW;
	
	//INITIALIZATION OF THE OBJECTS
	private Label volume;
	private Slider levelvolume;
	private Label difficultyLabel;
	private Stage mainStage;
	private Welcome sceneWelcome;
	private MusicPlayer music;
	
	//INITIALIZATION OF THE WHOLE DATA
	private double volumeGame;
	private int enemyDifficulty;
	private int volum_min;
	private int volum_max;
	
	//INITIALIZATION OF CONTAINERS
	private HBox sound;
	private HBox difficulty;
	private VBox order;
	
	
	/**
	 * WINDOW BUILDER WITH OPTIONS
	 * @param ROOT DATA TYPE GROUP
	 * @param WIDTH GIVES THE SIZE OF THE SCREEN WIDTH
	 * @param HEIGHT GIVES THE SCREEN HIGH SIZE
	 */
	public Options(Group root, double width, double height,Stage mainStage, Welcome sceneWelcome,MusicPlayer music) {
		super(root);
		    
	        volum_min=0;
	        volum_max=10;
	        this.mainStage=mainStage;
	        this.sceneWelcome=sceneWelcome;
	        this.music=music;
	        
	        setLevelvolume(new Slider(volum_min,volum_max,5));
	        getLevelvolume().setShowTickMarks(true);
	        getLevelvolume().setShowTickLabels(true);
	        getLevelvolume().setMajorTickUnit(2f);
	        getLevelvolume().setBlockIncrement(1f);
	        
	        difficultyLabel = new Label("DIFICULTAD: ");     
	        levelDifficultyeasy = new Button("FACIL");
	        levelDifficultymiddle = new Button("MEDIO");
	        levelDifficultyhard = new Button("DIFICIL");
	        levelDifficultyeasy.setOnAction(this);
	        levelDifficultymiddle.setOnAction(this);
	        levelDifficultyhard.setOnAction(this);
	        
	        enemyDifficulty = 5;
	     
	        difficulty=new HBox(difficultyLabel,levelDifficultyeasy,levelDifficultymiddle,levelDifficultyhard);
	        difficulty.setPadding(new Insets(10.0));
	        accept=new Button("GUARDAR CAMBIOS");
	        accept.setOnAction(this);
	    	returnW=new Button("VOLVER AL MENU");
	    	returnW.setOnAction(this);
	    	accept.setMaxWidth(200);
	    	returnW.setMaxWidth(200);
	        sound=new HBox(); 
	        volume = new Label("VOLUMEN:");
	        sound.setPadding(new Insets(10.0));
	        sound.getChildren().addAll(volume,getLevelvolume());
	        order=new VBox(sound,difficulty,accept,returnW); 
	        order.setStyle("-fx-background-color: #330011");
	        order.setMinSize(1000, 600);
	        order.setMaxSize(1000, 600);
	    	
	        
	        VBox.setMargin(sound, new Insets(250,0,0,350));
	        VBox.setMargin(difficulty, new Insets(0,0,0,350));
	        VBox.setMargin(accept, new Insets(0,0,0,380));
	        VBox.setMargin(returnW, new Insets(0,0,0,380));
	        root.getChildren().add(order);
	        root.setStyle("-fx-base: rgb(153,0,23);");
	}
	
	/**
	 * @return the levelDifficultyeasy
	 */
	public Button getLevelDifficultyeasy() {
		return levelDifficultyeasy;
	}

	/**
	 * @return the levelDifficultymiddle
	 */
	public Button getLevelDifficultymiddle() {
		return levelDifficultymiddle;
	}

	/**
	 * @return the levelDifficultyhard
	 */
	public Button getLevelDifficultyhard() {
		return levelDifficultyhard;
	}

	/**
	 * @return the accept
	 */
	public Button getAccept() {
		return accept;
	}

	/**
	 * @return the returnW
	 */
	public Button getReturnW() {
		return returnW;
	}
	
	/**
	 * @return the volumeGame
	 */
	public double getVolumeGame() {
		return volumeGame;
	}

	/**
	 * @param volumeGame the volumeGame to set
	 */
	public void setVolumeGame(int volumeGame) {
		this.volumeGame = volumeGame;
	}

	/**
	 * @return the enemyDifficulty
	 */
	public int getEnemyDifficulty() {
		return enemyDifficulty;
	}

	/**
	 * @param enemyDifficulty the enemyDifficulty to set
	 */
	public void setEnemyDifficulty(int enemyDifficulty) {
		this.enemyDifficulty = enemyDifficulty;
	}
	
	public void saveInformation(double i) {
		music.updateVolume(i/10);
	}

	public Slider getLevelvolume() {
		return levelvolume;
	}

	public void setLevelvolume(Slider levelvolume) {
		this.levelvolume = levelvolume;
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(getReturnW())) {
			mainStage.setScene(sceneWelcome);
		}else if(event.getSource().equals(getAccept())) {
			saveInformation((getLevelvolume().getValue()));
		}else if(event.getSource().equals(getLevelDifficultyeasy())) {
			setEnemyDifficulty(5);
		}else if(event.getSource().equals(getLevelDifficultymiddle())) {
			setEnemyDifficulty(10);
		}else if(event.getSource().equals(getLevelDifficultyhard())) {
			setEnemyDifficulty(15);
		}
	}
}

