
package Interface;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Classification extends Scene implements EventHandler<ActionEvent>{

	//INITIALIZATION OF THE OBJECTS
	private Stage mainStage;
	private Welcome sceneWelcome;
	private Button returnW;
	private Archivador archive;
	
	//CREATE A BOX AND TAKE ALL THE KEYS THERE
	private Label data;
	
	//ORGANIZE BOTH BOXES IN ONE
	private HBox total;
	
	
	public  Classification(AnchorPane root, double width, double height,Stage mainStage, Welcome sceneWelcome,Archivador archivo) {
		super(root);
		this.archive=archivo;
		root.setMinSize(width, height);
		total=new HBox();
		archive.saveData();
		data=new Label(archive.data());
		data.setStyle("-fx-base: rgb(153,0,23);");
		
		this.mainStage=mainStage;
		this.sceneWelcome=sceneWelcome;
		returnW=new Button("VOLVER AL MENU");
		returnW.setMaxWidth(200);
		returnW.setStyle("-fx-background-color: rgb(108,21,0);");
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		returnW.setOnAction(this);
		
	
		AnchorPane.setTopAnchor(data, 100.0);
		AnchorPane.setRightAnchor(data, 450.0);
		AnchorPane.setTopAnchor(total, 30.0);
		AnchorPane.setLeftAnchor(total, 20.0);
		AnchorPane.setBottomAnchor(returnW, 10.0);
		AnchorPane.setRightAnchor(returnW, 10.0);
		
		root.getChildren().addAll(data,total,returnW);
        root.setStyle("-fx-background-color: #330011");
	}
	
	public void update(String data) {
		this.data.setText(data);
	}
	
	/**
	 * @return the returnW
	 */
	public Button getReturnW() {
		return returnW;
	}
	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(getReturnW())) {
			mainStage.setScene(sceneWelcome);
		}
	}	
	

}
