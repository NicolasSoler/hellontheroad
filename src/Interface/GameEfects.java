package Interface;

import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class GameEfects {

	private Media musicCoin1;
	private Media musicCoin2;
	private Media musicCoin3;
	private MediaPlayer playeCoin1;
	private MediaPlayer playeCoin2;
	private MediaPlayer playeCoin3;
	private boolean run1;
	private boolean run2;
	private boolean run3;

	public GameEfects() {
		
		run1=false;
		run2=false;
		run3=false;
		
		musicCoin1=new Media(new File("src/Images/COIN.mp3").toURI().toString());
		musicCoin2=new Media(new File("src/Images/COIN.mp3").toURI().toString());
		musicCoin3=new Media(new File("src/Images/COIN.mp3").toURI().toString());
		
		playeCoin1=new MediaPlayer(musicCoin1);
		playeCoin2=new MediaPlayer(musicCoin2);
		playeCoin3=new MediaPlayer(musicCoin3);
	}
	
	/**
	 * REPRODUCE THE COIN MUSIC
	 */
	public void playCoin() {
		if(!run1) {
			run1=true;
			playeCoin1.play();
		}else if(!run2) {
			run2=true;
			playeCoin2.play();
		}else {
			run3=true;
			playeCoin3.play();
		}
	}
	
	/**
	 * PAUSE THE COIN MUSIC
	 */
	public void pauseCoin() {
		if(run1) {
			run1=false;
			playeCoin1.stop();
		}else if(run2) {
			run2=false;
			playeCoin2.stop();
		}else {
			run3=false;
			playeCoin3.stop();
		}
	}
}
