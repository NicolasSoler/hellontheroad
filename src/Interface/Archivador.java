package Interface;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeMap;

public class Archivador {

	//INITIALIZATION OF THE STRING LIST
	TreeMap<Integer,String>  listaTexto;
	
	public Archivador() {
		listaTexto=new TreeMap<Integer, String>(java.util.Collections.reverseOrder());
	}
	
	/**
	 * SAVE THE DATA IN THE LIST
	 */
	public void saveData() {
		boolean intento = true;
		int posicion =0;
		try {
			FileReader archivo = new FileReader("src/InteractionsGame/Puntaje");
			@SuppressWarnings("resource")
			BufferedReader cadena=new BufferedReader(archivo);
			String linea=cadena.readLine();
			
			while (intento) {
				if(linea==null) {
					intento =false;
				}else {
					String [] puntuacion= linea.split("\\*");
					listaTexto.put(Integer.parseInt(puntuacion[1]), puntuacion[0]);
					
					if(posicion!=10){
						linea=cadena.readLine();
						posicion=posicion+1;
					}
				}
			}
		}catch(FileNotFoundException e) {
			System.out.println("EL ARCHIVO NO EXISTE");
			
		}catch(IOException e){
			System.out.println("ERROR LEYENDO EL ARCHIVO: "+"Interface/Puntaje");
		
		}
	}
	
	/**
	 * ADD THE DATA IN THE LIST
	 * @param puntos
	 * @param nombre
	 */
	public void add(int puntos, String nombre) {
		if(listaTexto.size()<10&&!nombre.equals("")) {
			listaTexto.put(puntos, nombre);
		}
		updateText();
	}
	
	/**
	 * UPDATE THE TEXT IN THE .TXT
	 */
	public void updateText() {
	
		try {
			File archivo = new File("src/InteractionsGame/Puntaje");
			FileWriter editor = new FileWriter(archivo,false);
			BufferedWriter linea = new BufferedWriter(editor);
			
			String temporal="";
			linea.write("");
			@SuppressWarnings("rawtypes")
			Iterator iterator = listaTexto.keySet().iterator();
			while (iterator.hasNext()) {
			Object key = iterator.next();
			temporal=temporal+listaTexto.get(key)+"*"+key+"\n";
			}
			linea.write(temporal);
			
			linea.close();
			
		}catch(FileNotFoundException e) {
			System.out.println("EL ARCHIVO NO EXISTE");
			
		}catch(IOException e){
			System.out.println("ERROR LEYENDO EL ARCHIVO "+ "src/InteractionsGame/Puntaje");
			
		}
	}
	
	/**
	 * SHOW THE DATA THAT IS SAVED
	 * @return
	 */
	public String data() {
		String total="";
		
		@SuppressWarnings("rawtypes")
		Iterator iterator = listaTexto.keySet().iterator();
		while (iterator.hasNext()) {
		Object key = iterator.next();
		total=total+listaTexto.get(key)+"  "+key+"\n";
		}
		return total;
	}
}
