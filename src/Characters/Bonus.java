package Characters;

import javafx.scene.image.Image;

public class Bonus extends Characters {

	/**
	 * BONUS BUILDER BUILDER WITH HIS DEFAULT VALUES
	 */
	public Bonus() {
		super();
		setCarro(new Image("images/auto2.png",getSizeX(),getSizeY(),false,false));
		setNave(new Image("Images/nave5.png",getSizeX(),getSizeY(),true,false));
		setSkin(getCarro());
		double x =0;
		while (x<110&&x<600) {
			x=(Math.random()*600);
		}
		setPosX(x);
		setPosY((Math.random()*1000)*-1);
		setSpeed(5);
	}
	

	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE SHIP IN CAR
	 */
	public void transformShip() {
    	setSkin(getNave());
    	
	}
	
	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE CAR IN SHIP
	 */
	public void transformCar() {
		setSizeX(60);
		setSizeY(160);
    	setSkin(getCarro());}
}
