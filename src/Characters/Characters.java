package Characters;

import javafx.scene.image.Image;

public class Characters {

	private Image skin;
	private boolean life;
	private double posX;
	private double posY;
	private double maxX;
	private double maxY;
	private int speed;
	private double sizeX;
	private double sizeY;
	private boolean address;
	private Image carro;
	private Image nave;
	private boolean action;
	
	
	/**
	 * CREATE THE CHARACTER WITH VALUES IN 0
	 */
	public Characters() {
		
		life=true;
		skin = null;
		posX=0;
		posY=0;
		maxX=0;
		maxY=0;
		sizeX=60;
		sizeY=160;
		speed=5;
		address = true;
		action=true;
	}

	/**
	 * ASSIGN THE IMAGE OF DEATH
	 */
	public void dead() {
		setSkin(new Image("images/explosion.png",100,100,true,false));
	}
	public void bonus() {
		sizeX=80;
		sizeY=80;
		setSkin(new Image("images/bonus.png",100,100,true,false));
	}
	public Image getCarro() {
		return carro;
	}

	public void setCarro(Image carro) {
		this.carro = carro;
	}

	public Image getNave() {
		return nave;
	}

	public void setNave(Image nave) {
		this.nave = nave;
	}

	/**
	 * SHOWS THE DEFINED SIZE X FOR THE SKIN
	 * @return THE SIZE X
	 */
	public double getSizeX() {
		return sizeX;
	}

	/**
	 * CHANGE THE DEFINED SIZE X FOR THE SKIN
	 * @param NEW SIZE X
	 */
	public void setSizeX(double size) {
		this.sizeX = size;
	}
	
	/**
	 * SHOWS THE DEFINED SIZE Y FOR THE SKIN
	 * @return THE SIZE Y
	 */
	public double getSizeY() {
		return sizeY;
	}

	/**
	 * CHANGE THE DEFINED SIZE X FOR THE SKIN
	 * @param NEW SIZE X
	 */
	public void setSizeY(double size) {
		this.sizeY = size;
	}

	/**
	 * SHOW IF YOU HAVE ALLOWED TO MOVE
	 * @return THE ACTION
	 */
	public boolean isAction() {
		return action;
	}

	/**
	 * CHANGE THE STATE OF THE MOVEMENT PERMIT
	 * @param THE MOVEMENT PERMIT
	 */
	public void setAction(boolean action) {
		this.action = action;
	}
	
	/**
	 * SHOW THE SPEED OF THE CHARACTER (INT)
	 * @return THE SPEED OF THE CHARACTER
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * CHANGE THE SPEED OF THE CHARACTER 
	 * @param THE SPEED OF THE CHARACTER(INT)
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * SHOW THE CHARACTER SKIN (IMAGE)
	 * @return SKIN (IMAGE)
	 */
	public Image getSkin() {
		return skin;
	}

	/**
	 * CHANGE THE IMAGE OF THE CHARACTER (IMAGE)
	 * @param RECEIVE THE SKIN (IMAGE) BY WHICH THE IMAGE WILL BE UPDATED
	 */
	public void setSkin(Image skin) {
		this.skin = skin;
	}

	/**
	 * SHOW THE POSITION IN X (DOUBLE)
	 * @return POSITION IN X (DOUBLE)
	 */
	public double getPosX() {
		return posX;
	}

	/**
	 * CHANGE THE POSITION IN X
	 * @param RECIVE POSITION IN X (DOUBLE)
	 */
	public void setPosX(double posX) {
		this.posX = posX;
	}

	/**
	 *  SHOW THE POSITION IN Y (DOUBLE)
	 * @return POSITION IN Y (DOUBLE)
	 */
	public double getPosY() {
		return posY;
	}

	/**
	 * CHANGE THE POSITION IN Y
	 * @param RECIVE POSITION IN Y (DOUBLE)
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}

	/**
	 * SHOW THE MAXIMUM IN X (DOUBLE)
	 * @return THE MAXIMUM IN X (DOUBLE)
	 */
	public double getMaxX() {
		maxX=posX+sizeX;
		return maxX;
	}

	/**
	 * SHOW THE MAXIMUM IN Y (DOUBLE)
	 * @return THE MAXIMUM IN Y (DOUBLE)
	 */
	public double getMaxY() {
		maxY=posY+sizeY;
		return maxY;
	}
	
	/**
	 * MOVES THE CHARACTER TOWARDS A SPEED DEPENDENT AMOUNT
	 */
	public void moveUp() {
		if(action) {
			this.posY = posY-speed;
		}
	}
	
	/**
	 * MOVES THE CHARACTER DOWN A DEPENDENT AMOUNT OF SPEED
	 */
	public void moveDown() {
		if(action) {
			this.posY = posY+speed;
		}
	}
	
	/**
	 * MOVES THE CHARACTER TO THE RIGHT A DEPENDENT AMOUNT OF SPEED
	 */
	public void moveRight() {
		if(action) {
			this.posX = posX+speed;
		}
	}
	
	/**
	 * MOVE THE CHARACTER TO THE LEFT A DEPENDENT AMOUNT OF SPEED
	 */
	public void moveLeft() {
		if(action) {
			this.posX = posX-speed;
		}
	}

	/**
	 * SHOW THE ADDRES
	 * @return the address
	 */
	public boolean isAddress() {
		return address;
	}

	/**
	 *  SHOW THE POSITION IN X
	 * @param address the address to set
	 */
	public void setAddress(boolean address) {
		this.address = address;
	}
	
	/**
	 * @return the life
	 */
	public boolean isLife() {
		return life;
	}

	/**
	 * @param life the life to set
	 */
	public void setLife(boolean life) {
		this.life = life;
	}

	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE SHIP IN CAR
	 */
	public void transformShip() {
	}
	
	
	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE CAR IN SHIP
	 */
	public void transformCar() {
	}
}
