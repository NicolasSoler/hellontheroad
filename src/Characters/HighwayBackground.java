package Characters;

import javafx.scene.image.Image;

public class HighwayBackground {
	
	private Image background;
	private double posX;
	private double posY;
	private int speed;
	
	/**
	 * CONSTRUCTOR OF THE ROAD WHICH WILL BE UPDATED EVERY THREE IMAGES BY SCREEN
	 */
	public HighwayBackground(Image temporary) {
		background = temporary;
		speed = 0;
	}
	
	/**
	 * CONSTRUCTOR OF THE ROAD WHICH WILL BE UPDATED EVERY THREE IMAGES BY SCREEN
	 */
	public HighwayBackground(Image temporary,double Y) {
		background = temporary;
		posY=Y;
		speed = 0;
	}
	
	/**
	 * MOVES THE HIGHWAYBACKGROUND TOWARDS A SPEED DEPENDENT AMOUNT
	public void moveUp() {
		this.posY = posY-speed;
	}
	
	/**
	 * MOVES THE HIGHWAYBACKGROUND DOWN A DEPENDENT AMOUNT OF SPEED
	 */
	public void moveDown() {
			this.posY = posY+speed;
	}
	
	/**
	 * SHOW THE POSITION IN X (DOUBLE)
	 * @return POSITION IN X (DOUBLE)
	 */
	public double getPosX() {
		return posX;
	}

	/**
	 * CHANGE THE POSITION IN X
	 * @param RECIVE POSITION IN X (DOUBLE)
	 */
	public void setPosX(double posX) {
		this.posX = posX;
	}

	/**
	 *  SHOW THE POSITION IN Y (DOUBLE)
	 * @return POSITION IN Y (DOUBLE)
	 */
	public double getPosY() {
		return posY;
	}

	/**
	 *  CHANGE THE POSITION IN Y
	 * @param RECIVE POSITION IN Y (DOUBLE)
	 */
	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	/**
	 * SHOW THE SPEED OF THE CHARACTER (INT)
	 * @return THE SPEED OF THE CHARACTER
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * CHANGE THE SPEED OF THE CHARACTER 
	 * @param THE SPEED OF THE CHARACTER(INT)
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * METHOD THAT RECEIVES AND RETURNS THE IMAGE TO BE USED IN BACKGROUND
	 * @return THE IMAGE TO BE USED IN BACKGROUND
	 */
	public Image getBackground() {
		return background;
	}
	/**
	 * METHOD THAT GIVES ACCESS TO THE IMAGE TO BE USED IN THE BACKGROUND
	 * @param TO THE IMAGE TO BE USED IN THE BACKGROUND
	 */
	public void setBackground(Image background) {
		this.background = background;
	}
}
