package Characters;
 
import javafx.scene.image.Image;

public class Protagonist extends Characters{

	private double fury;
	private int score;
	/**
	 * CHARACTER'S CONSTRUCTOR WITH THE PREDETERMINATED VALUES OF THE PLAYER
	 */
	public Protagonist() {
		super();
		setCarro(new Image("images/auto1.png",getSizeX(),getSizeY(),false,false));
		setNave(new Image("Images/nave6.png",getSizeX(),getSizeY(),true,false));
		setSkin(getCarro());
		setPosX(320);
		setPosY(700);
		setSpeed(20);
		fury = 100;
		score=0;
	}
	
	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE SHIP IN CAR
	 */
	public void transformShip() {
		//setSkin(new Image("Images/transformacion.png",100,100,true,false));
    	setSkin(getNave());
	}
	
	/**
	 * METHOD THAT IS IN CHARGE OF TRANSFORMING THE CAR IN SHIP
	 */
	public void transformCar() {
		setSizeX(55);
		setSizeY(160);
    	setSkin(getCarro());
	}
	
	/**
	 * METHOD WITHOUT RETURN THAT CALCULATES THE REMAINING FURY OF THE PLAYER
	 */
	public void loseFury() {
		if(fury<200) {
			this.fury=fury-0.1;
		}else if(fury<600){
			this.fury=fury-0.5 ;
		}else if(fury<1000){
			this.fury=fury-1 ;
		}
	}
	
	/**
	 * SHOW THE SCORE OF THE PROTAGONIST
	 * @return THE SCORE
	 */
	public int getScore() {
		return score;
	}

	/**
	 *  CHANGE THE SCORE OF THE PROTAGONIST
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * METHOD RESPONSIBLE FOR RETURNING THE CURRENT FURY OF THE PLAYER
	 * @return THE CURRENT FURY OF THE PLAYER
	 */
	public double getFury() {
		return fury;
	}

	/**
	 * METHOD RESPONSIBLE FOR RECEIVING THE PLAYER'S FURY AND ALLOWING ACCESS TO THE PLAYER
	 * @param THE PLAYER'S FURY
	 */
	public void setFury(double fury) {
		this.fury = fury;
	}
}
