package InteractionsGame;
import Interface.Archivador;
import Interface.Classification;
import Interface.MusicPlayer;
import Interface.Welcome;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class EndScore implements EventHandler<ActionEvent>{

	//TEXT INITIALIZER
	private Label puntos; 
	private Label salidaPuntos; 
	private Label historia;
	private TextField nickname;
	private Label nick;
	
	//INITIALIZER OF OBJECTS
	private ImageView fondo;
	private Archivador archivo;
	private Welcome sceneWelcome;
	private Stage mainStage;
	private AnchorPane ventanaJuego;
	private MusicPlayer music;
	private Classification classification;
	
	//INITIALIZER OF BUTTONS AND EXTRA
	@SuppressWarnings("unused")
	private boolean one;
	private Button returnW;
	private Button save;
	
	/**
	 * CONSTRUCTOR OF THE FINAL SCREEN
	 * @param salidaPuntos
	 * @param nombre
	 * @param contraseņa
	 * @param archivo
	 * @param mainStage
	 * @param sceneWelcome
	 */
	public EndScore( Label salidaPuntos, 
		Label nombre, String contraseņa, Archivador archivo,
		Stage mainStage,Welcome sceneWelcome,MusicPlayer music,Classification classification) {
		super();
		
		this.classification=classification;
		this.sceneWelcome=sceneWelcome;
		this.mainStage=mainStage;
		
		this.music=music;
		fondo = new ImageView("images/endGame.png");
		
		one=true;
		save = new Button("GUARDAR PUNTAJE");
		this.archivo=archivo;
		archivo.saveData();
		historia = new Label(archivo.data());
		save.setOnAction(this);
		save.setMinSize(200, 50);
		save.setDefaultButton(false);
		save.setStyle("-fx-base: rgb(153,0,23);");
		nickname=new TextField("");
		nick=new Label("NOMBRE: ");
		ventanaJuego = new AnchorPane();
		returnW=new Button("VOLVER AL MENU");
    	returnW.setMinSize(200, 50);
    	returnW.setDefaultButton(false);
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		returnW.setDefaultButton(false);
		returnW.setOnAction(this);
		this.puntos = new Label("PUNTOS: ");
		this.salidaPuntos = new Label(salidaPuntos.getText());
		ventanaJuego.setMinSize(750, 850);

		this.nick.setStyle("-fx-base: rgb(153,0,23);");
		this.nickname.setStyle("-fx-base: rgb(153,50,45);");
		this.historia.setStyle("-fx-base: rgb(153,0,23);");
		this.puntos.setStyle("-fx-base: rgb(153,0,23);");
		this.salidaPuntos.setStyle("-fx-base: rgb(153,0,23);");
		
		AnchorPane.setTopAnchor(this.puntos, 350.0);
		AnchorPane.setTopAnchor(this.salidaPuntos, 350.0);
		AnchorPane.setBottomAnchor(this.returnW, 20.0);
		AnchorPane.setTopAnchor(historia, 450.0);
		AnchorPane.setTopAnchor(nickname, 350.0);
		AnchorPane.setTopAnchor(nick, 350.0);
		AnchorPane.setBottomAnchor(save, 80.0);

		AnchorPane.setRightAnchor(save, 20.0);
		AnchorPane.setLeftAnchor(historia, 320.0);
		AnchorPane.setRightAnchor(nick, 490.0);
		AnchorPane.setRightAnchor(nickname, 300.0);
		AnchorPane.setRightAnchor(this.returnW, 20.0);
		AnchorPane.setRightAnchor(this.puntos, 230.0);
		AnchorPane.setRightAnchor(this.salidaPuntos, 190.0);

		ventanaJuego.getChildren().addAll(fondo,this.puntos,
				this.salidaPuntos,this.returnW,this.historia
				,this.nick,this.nickname,save);
		ventanaJuego.setStyle("-fx-background-color: #000000");
		
	}
	
	/**
	 * SHOW THE RETURN BUTTON
	 * @return
	 */
	public Button getReturnW() {
		return returnW;
	}

	/**
	 * SHOW THE ORGANIZER
	 * @return
	 */
	public AnchorPane getVentanaJuego() {
		return ventanaJuego;
	}

	/**
	 * SHOW THE POINTS IN TEXT
	 * @return
	 */
	public Label getSalidaPuntos() {
		return salidaPuntos;
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(save)) {
			one=false;
			archivo.add(Integer.parseInt(salidaPuntos.getText()), nickname.getText());
			mainStage.setScene(sceneWelcome);
			classification.update(archivo.data());
			music.pauseEndGame();
			music.playMenu();
		}else if(event.getSource().equals(returnW)) {
			mainStage.setScene(sceneWelcome);
			music.pauseEndGame();
			music.playMenu();
		}
		
	}
	
}
