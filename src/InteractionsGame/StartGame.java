package InteractionsGame;

import java.util.Timer;
import java.util.TimerTask;

import Characters.Bonus;
import Characters.ChangeBonus;
import Characters.Characters;
import Characters.Monster;
import Interface.Archivador;
import Interface.Classification;
import Interface.MusicPlayer;
import Interface.Options;
import Interface.Welcome;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class StartGame extends Scene implements EventHandler<KeyEvent>{

	//INITIALIZATION OF THE OBJECTS
	private Canvas canvasLevel;
	private MusicPlayer music;
	private GraphicsContext context;
	private Highway movingRoads;
	private Score point;
	private Group principal;
	private String level;
	private Archivador archive;
	
	//INITIALIZATION OF BASIC DATA
	private int background;
	private int backgroundPluss;
	private boolean finale;
	private boolean moveEnemy;
	private int advance;
	private int poslevel;
	private int timeBonus;
	private int timeDiff;
	private boolean playMusic;
	
	/**
	 * CONSTRUCTOR OF THE STARTGAME CLASS WITH ITS PREDETERMINED VALUES
	 * @param ROOT IN WHICH THE DATA WILL BE STORED
	 * @param WIDTH THAT IS GOING TO HAVE THE SCREEN
	 * @param HEIGHT THAT WILL HAVE THE SCREEN
	 */
	public StartGame(Group root, double width, double height,Options option,Stage mainStage,Welcome sceneWelcome,Archivador archive,MusicPlayer music,Classification classification) {
		super(root);
		
		playMusic=false;
		this.music=music;
		music.pauseMenu();
		music.playGame();
		timeDiff = 1500;
		this.archive=archive;
		timeBonus=(int)(Math.random()*200)+20;
		canvasLevel= new Canvas(width,height);
		movingRoads = new Highway(option);
		poslevel=0;
		level="";
		finale = false;
		point = new Score(archive,mainStage,sceneWelcome,this,music,classification);
		movingRoads.createEnemies();
		moveEnemy=false;
		selecLevel();
		principal=root;
		root=null;
		principal.getChildren().addAll(canvasLevel,getPoint().getVentanaGeneral());
		setContext(canvasLevel.getGraphicsContext2D());
		this.setOnKeyPressed(this);
		archive.saveData();
	}
	
	/**
	 * METHOD IN CHARGE OF RUNNING THE GAME
	 */
	public void runLevel() {
		new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				if(!finale){
					if(movingRoads.getPlayer().getFury()<0) {
						movingRoads.getPlayer().setAction(false);
					}
					point.setVelocidadInt(movingRoads.getVelocidad());
					point.actualizarDatos();
					//SHOWS THE FIRST IMAGE OF THE FUND
					context.drawImage(movingRoads.background(background),
										movingRoads.getPosX(background),
										movingRoads.getPosY(background));
					
					//SHOW THE SECOND IMAGE OF THE FUND
					context.drawImage(movingRoads.background(backgroundPluss), 
										movingRoads.getPosX(backgroundPluss),
										movingRoads.getPosY(backgroundPluss));
					

					//MANAGE THE MOVEMENT DEPENDING ON IF THE PLAYER IS "LIVE"
					if(movingRoads.getPlayer().isAction()) {
						
						new Timer().schedule(new TimerTask() {
					        @Override
					        public void run() {
					        	if(movingRoads.getListImage().get(background).getSpeed()<=20) {
					        		movingRoads.getListImage().get(background).setSpeed(movingRoads.getListImage().get(background).getSpeed()+1);
					        		movingRoads.getListImage().get(backgroundPluss).setSpeed(movingRoads.getListImage().get(backgroundPluss).getSpeed()+1);
					        		moveEnemy=true;
					        	}
					        }
					    }, 1000);
						
						if(moveEnemy) {
							deadEnemy();
							movingRoads.moveCharacters();
						}
						
						difficulty();
						addEnemy(movingRoads.getListCharacters().size());
						selecLevel();
						bonusLevel();
						movingRoads.createEnemies();
						movingRoads.deleteEnemy();
						movingRoads.moveBackground(background,backgroundPluss);
						movingRoads.getPlayer().loseFury();
						point.setTiempoInt(movingRoads.getPlayer().getFury());
						point.setPuntosInt(movingRoads.getPlayer().getScore());
						point.getPantallaFinal().getSalidaPuntos().setText(""+movingRoads.getPlayer().getScore());
						point.setVelocidadInt(movingRoads.getListCharacters().get(0).getSpeed());
			        	
					}
					
					//MANAGE THE END OF THE GAME SCREEN
					if(!movingRoads.getPlayer().isAction()) {
						new Timer().schedule(new TimerTask() {
					        @Override
					        public void run() {
					        	finale=true;
					        }
					    }, 1000);
					}	
				}else {
					stop();
					showEnd();
				}
			}
		}.start();
	}
	
	/** 
	 * METHOD IN CHARGE OF RUNNING THE CHARACTERES
	 */
	public void runCharacter() {
		new AnimationTimer() {
			@Override
			public void handle(long now) {
				
				if(!finale) {
					//SHOW THE ENEMIES CREATED IN THE GAME
					for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
						context.drawImage(movingRoads.getListCharacters().get(i).getSkin(),
								movingRoads.getListCharacters().get(i).getPosX(), 
								movingRoads.getListCharacters().get(i).getPosY());
					}
					
					//MOVES AND INTERCALLS THE IMAGES OF THE FUND ACCORDING TO ITS POSITION
					if(movingRoads.getListImage().get(background).getPosY()>=850) {
						movingRoads.getListImage().get(background).setPosY(-850);
						if(background==4) {
							background=2;
						}
					}
					if(movingRoads.getListImage().get(backgroundPluss).getPosY()>=850) {
						movingRoads.getListImage().get(backgroundPluss).setPosY(-850);
						if(backgroundPluss==5) {
							backgroundPluss=3;
						}
					}
					
					//MOVEMENT LIMITER X AXIS AND ALSO CONSEQUENCE OF COLLISION
					if((!movingRoads.isInBonus())&&(movingRoads.getPlayer().getPosX()<=100&&movingRoads.getPlayer().getPosY()>=0.0
							&&movingRoads.getPlayer().getPosY()<=780.0
							||movingRoads.getPlayer().getPosX()>=600&&movingRoads.getPlayer().getPosY()>=0.0
							&&movingRoads.getPlayer().getPosY()<=780.0)) {
						
						runMusic();
						music.pauseGame();
						music.playExplosion();
						music.playEndGame();
						movingRoads.getPlayer().setAction(false);
						movingRoads.getPlayer().dead();
					}
					
					//COLLISION WITH THE ENEMY
					if(movingRoads.collisionPlayer()!=null) {
						collisionEnemy(movingRoads.collisionPlayer());
					}
					movingRoads.collisionEnemigos();
					
					//SHOW THE PLAYER
					context.drawImage(movingRoads.getPlayer().getSkin(),
										movingRoads.getPlayer().getPosX(), 
										movingRoads.getPlayer().getPosY());
					
				}else {
					stop();
				}
			}
		}.start();
	}
	
	public void runMusic() {
		new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				if(playMusic) {
					
					music.getGameEfects().playCoin();
					
					new Timer().schedule(new TimerTask() {
				        @Override
				        public void run() {
				        	music.getGameEfects().pauseCoin();
				        	playMusic=false;
				        }
				    }, 500);
				}
			}
		}.start();
	}
	
	/**
	 * CHOOSE THE GAME MODE YOU ARE IN
	 */
	public void selecLevel() {
		
		if(level.equals("PrDurlAO")) {
			background=2;
			backgroundPluss=3;
			for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
				if(movingRoads.isBonus()) {
					movingRoads.getListCharacters().get(i).bonus();
				}else {
					if(movingRoads.getListCharacters().get(i).isLife()) {
						movingRoads.getListCharacters().get(i).transformShip();
					}
				}
			}
			movingRoads.getPlayer().transformShip();
		}else {
			background=0;
			backgroundPluss=1;
			for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
				if(movingRoads.getListCharacters().get(i).isLife()) {
					movingRoads.getListCharacters().get(i).transformCar();
				}
			}
			movingRoads.getPlayer().transformCar();
		}
		
	}
	
	/**
	 * SHOW THE FINAL SCREEN
	 */
	public void showEnd() {
		principal.getChildren().clear();
		principal.getChildren().addAll(point.getPantallaFinal().getVentanaJuego());
	}
	
	/**
	 * INCREASE DIFFICULTY LAST X TIME
	 */
	public void difficulty() {
		
		if(timeDiff>=0) {
			timeDiff=timeDiff-1;
		}else {
			timeDiff=1500;
			movingRoads.climbDifficulty();
		}
	}
	
	/**
	 * RANDOMLY ASSIGNS THE APPEARANCE OF THE BONUS
	 */
	public void bonusLevel() {
		if(advance<timeBonus) {
			advance+=1;
		}else {
			advance=0;
			if(poslevel==0) {
				movingRoads.setBonus(true);
				timeBonus=1000;
			}else {
				music.pauseBonus();
				music.playGame();
				movingRoads.getListCharacters().clear();
				movingRoads.getPlayer().setPosX(320);
				movingRoads.setInBonus(false);
				timeBonus=(int)(Math.random()*2000)+20;
				movingRoads.setActivate(true);
				movingRoads.setBonus(false);
				poslevel=0;
				level="";
			}
		}
	}
	
	/**
	 * CALCULATE THE COLLISION BETWEEN ENEMIES
	 * @param enemigo
	 */
	public void collisionEnemy(Characters enemigo) {

		if(enemigo.isLife()) {
			if(!movingRoads.isActivate()) {
				movingRoads.getPlayer().setScore(movingRoads.getPlayer().getScore()+70);
				movingRoads.getPlayer().setFury(movingRoads.getPlayer().getFury()+10);
				movingRoads.getListCharacters().remove(enemigo);
				playMusic=true;
			}
			else if(enemigo instanceof Bonus) {
				movingRoads.getPlayer().setScore(movingRoads.getPlayer().getScore()+100);
				movingRoads.getPlayer().setFury(movingRoads.getPlayer().getFury()+50);
				movingRoads.getListCharacters().remove(enemigo);
				playMusic=true;
				
			}else if(enemigo instanceof Monster) {
				music.pauseGame();
				music.playExplosion();
				music.playEndGame();
				movingRoads.getPlayer().setAction(false);
				movingRoads.getPlayer().dead();
			}else if(enemigo instanceof ChangeBonus) {
				playMusic=true;
				music.pauseGame();
				music.playBonus();
				movingRoads.setInBonus(true);
				poslevel=1;
				level="PrDurlAO";
				for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
					if(getMovingRoads().getListCharacters().get(i) instanceof ChangeBonus) {
						getMovingRoads().getListCharacters().remove(i);
					}
				}
				movingRoads.setActivate(false);
				movingRoads.getListCharacters().remove(enemigo);
			}
				
				else {
				moveEnemy=false;
				movingRoads.getPlayer().setFury(movingRoads.getPlayer().getFury()-5);
				int auxL = 3;
	        		if(enemigo.getPosX()>=movingRoads.getPlayer().getPosX()) {
	        			while(auxL!=0) {
	        				enemigo.moveRight();
	            			movingRoads.getPlayer().moveLeft();
	            			auxL=auxL-1;
	            		}
	        		}else {
	        			while(auxL!=0) {
	        				enemigo.moveLeft();
	            			movingRoads.getPlayer().moveRight();
	            			auxL=auxL-1;
	            		}
	        		}
	        		new Timer().schedule(new TimerTask() {
				        @Override
				        public void run() {
				        	moveEnemy=true;
				        }
				    }, 100);
			}
		}
	}
		
	/**
	 * INCREASE THE AMOUNT OF ENEMIES THAT SHOW IN SCREEN
	 * @param cant
	 */
	public void addEnemy(int cant) {
		if(cant<6) {
			movingRoads.setEnemyAmount(cant+1);
		}
	}
	
	/**
	 * METHOD THAT CALCULATES THE DEATH OF THE ENEMIES
	 */
	public void deadEnemy() {
		for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
			if((movingRoads.getListCharacters().get(i).getPosX()<=100
					||movingRoads.getListCharacters().get(i).getPosX()>=600)) {
				movingRoads.getListCharacters().get(i).setLife(false);
				movingRoads.getListCharacters().get(i).dead();
			}
		}
	}
	
	@Override
	public void handle(KeyEvent event) {
		
		if(moveEnemy) {
			if(event.getCode().equals(KeyCode.W)||event.getCode().equals(KeyCode.UP)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(movingRoads.getPlayer().getPosY()>=360) {
					movingRoads.getPlayer().moveUp();
				}
			}else if(event.getCode().equals(KeyCode.D)||event.getCode().equals(KeyCode.RIGHT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				movingRoads.getPlayer().moveRight();
			}else if(event.getCode().equals(KeyCode.A)||event.getCode().equals(KeyCode.LEFT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				movingRoads.getPlayer().moveLeft();
			}else if(event.getCode().equals(KeyCode.S)||event.getCode().equals(KeyCode.DOWN)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(movingRoads.getPlayer().getPosY()<=750) {
					movingRoads.getPlayer().moveDown();
				}
			}
		}
	}
	
	/**
	 * SAVE THE DATA IN THE .TXT
	 * @param puntos
	 * @param nombre
	 */
	public void agregarJugador(int puntos, String nombre) {
		archive.add(puntos,nombre);
	}
	
	/**
	 * CHANGES THE GRAPHIC CONTEXT THAT SHOWS ON SCREEN
	 * @param GRAPHICAL CONTEXT OF THE CAVAS
	 */
	public void setContext(GraphicsContext context) {
		this.context = context;
	}
	
	/**
	 * SHOW IF THE GAME COMES TO ITS END
	 * @return the finale
	 */
	public boolean isFinale() {
		return finale;
	}

	/**
	 * CHANGE IF THE GAME COMES TO ITS END
	 * @param finale the finale to set
	 */
	public void setFinale(boolean finale) {
		this.finale = finale;
	}

	/**
	 * SHOW THE ROAD
	 * @return the movingRoads
	 */
	public Highway getMovingRoads() {
		return movingRoads;
	}

	/**
	 * SHOW THE FUND IN WHOLE
	 * @return the background
	 */
	public int getBackground() {
		return background;
	}

	/**
	 * SHOW THE FUND 2 IN WHOLE
	 * @return the backgroundPluss
	 */
	public int getBackgroundPluss() {
		return backgroundPluss;
	}
	
	/**
	 * SHOW THE POINTS SCREEN
	 * @return the point
	 */
	public Score getPoint() {
		return point;
	}
}
