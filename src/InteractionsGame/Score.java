package InteractionsGame;

import Interface.Archivador;
import Interface.Classification;
import Interface.MusicPlayer;
import Interface.Welcome;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Score extends HBox implements EventHandler<ActionEvent>{

	//TEXT INITIALIZER
	private Label puntos; 
	private Label salidaPuntos; 
	private Label salidaVelocidad;
	private ProgressBar salidaTiempo;
	private Label nombre;
	private Label tiempo;
	private Label velocidad;
	
	//INITIALIZER OF BUTTONS AND EXTRAS
	private Button returnW;
	private double tiempoDouble;
	private int pointInt;
	private int velocidadInt;
	
	//INITIALIZER OF OBJECTS
	private AnchorPane ventanaJuego;
	private EndScore pantallaFinal;
	private Stage mainStage;
	private Welcome sceneWelcome;
	private StartGame game;
	private MusicPlayer music;
	@SuppressWarnings("unused")
	private Classification classification;
	
	/**
	 * POINT SCREEN BUILDER
	 * @param archivo
	 * @param mainStage
	 * @param sceneWelcome
	 */
	public Score(Archivador archivo,Stage mainStage,Welcome sceneWelcomest,StartGame game,MusicPlayer music,Classification classification) {
		tiempoDouble =0;
		pointInt =0;
		this.classification=classification;
		this.music=music;
		this.game=game;
		velocidadInt =0;
		this.mainStage=mainStage;
		this.sceneWelcome=sceneWelcomest;
		tiempo = new Label("FURIA:");
		returnW=new Button("VOLVER AL MENU");
    	returnW.setMinSize(100, 100);
    	returnW.setDefaultButton(false);
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		returnW.setOnAction(this);
		velocidad = new Label("VELOCIDAD:");
		ventanaJuego = new AnchorPane();
		puntos = new Label("PUNTOS: ");
		salidaPuntos = new Label(""+00000);
		salidaVelocidad = new Label(""+velocidadInt);
		salidaTiempo= new ProgressBar(tiempoDouble);
		nombre = new Label("");
		velocidad.setStyle("-fx-base: rgb(153,0,23);");
		tiempo.setStyle("-fx-base: rgb(153,0,23);");
		nombre.setStyle("-fx-base: rgb(153,0,23);");
		salidaTiempo.setStyle("-fx-base: rgb(153,0,23);");
		puntos.setStyle("-fx-base: rgb(153,0,23);");
		salidaVelocidad.setStyle("-fx-base: rgb(153,0,23);");
		salidaPuntos.setStyle("-fx-base: rgb(153,0,23);");
		pantallaFinal = new EndScore( this.salidaPuntos, this.nombre,"",archivo,mainStage,sceneWelcome,music,classification );
		AnchorPane.setTopAnchor(tiempo, 20.0);
		AnchorPane.setTopAnchor(velocidad, 50.0);
		AnchorPane.setTopAnchor(nombre, 20.0);
		AnchorPane.setTopAnchor(salidaTiempo, 20.0);
		AnchorPane.setTopAnchor(puntos, 20.0);
		AnchorPane.setTopAnchor(salidaVelocidad, 50.0);
		AnchorPane.setTopAnchor(salidaPuntos, 20.0);
	
		AnchorPane.setLeftAnchor(tiempo, 300.0);
		AnchorPane.setLeftAnchor(velocidad, 300.0);
		AnchorPane.setLeftAnchor(nombre, 150.0);
		AnchorPane.setLeftAnchor(salidaTiempo, 400.0);
		AnchorPane.setLeftAnchor(puntos, 520.0);
		AnchorPane.setLeftAnchor(salidaVelocidad, 400.0);
		AnchorPane.setLeftAnchor(salidaPuntos, 610.0);
		
		ventanaJuego.getChildren().addAll(salidaTiempo,puntos,salidaPuntos,salidaVelocidad,nombre,tiempo,velocidad,returnW);
		ventanaJuego.setMinSize(750, 100);
		ventanaJuego.setStyle("-fx-background-color: #330011");
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource().equals(getReturnW())) {
			music.pauseGame();
			music.pauseBonus();
			music.pauseEndGame();
			music.playMenu();
			game.setFinale(true);
			mainStage.setScene(sceneWelcome);
		}
	}
	
	/**
	 * SHOW THE RETURN BUTTON
	 * @return the returnW
	 */
	public Button getReturnW() {
		return returnW;
	}

	/**
	 * SHOW THE ORGANIZER
	 * @return
	 */
	public AnchorPane getVentanaJuego() {
	return ventanaJuego;
	}
	
	/**
	 * SHOWS THE LABEL THAT CONTAINS THE SPEED
	 * @return
	 */
	public Label getSalidaVelocidad() {
		return salidaVelocidad;
	}
	
	/**
	 * SHOW THE LABEL THAT CONTAINS THE TIME
	 * @return
	 */
	public ProgressBar getSalidaTiempo() {
		return salidaTiempo;
	}
	
	/**
	 * SHOW THE END OF GAME SCREEN
	 * @return
	 */
	public EndScore getPantallaFinal() {
		return pantallaFinal;
	}

	/**
	 * UPDATE SCREEN DATA
	 */
	public void actualizarDatos() {
		salidaTiempo.setProgress(tiempoDouble/100);	
		salidaPuntos.setText(""+pointInt);
		salidaVelocidad.setText(""+velocidadInt);
	}
	
	/**
	 * SHOW THE SPEED
	 * @return the velocidadInt
	 */
	public int getVelocidadInt() {
		return velocidadInt;
	}

	/**
	 * CHANGE THE SPEED
	 * @param velocidadInt the velocidadInt to set
	 */
	public void setVelocidadInt(int velocidadInt) {
		this.velocidadInt = velocidadInt;
	}

	/**
	 * SHOW THE ORGANIZER
	 * @return the ventanaGeneral
	 */
	public AnchorPane getVentanaGeneral() {
		return ventanaJuego;
	}

	/**
	 * SHOW THE POINT TEXT
	 * @return the puntos
	 */
	public Label getPuntos() {
		return puntos;
	}

	/**
	 * SHOW THE POINTS
	 * @return the salidaPuntos
	 */
	public Label getSalidaPuntos() {
		return salidaPuntos;
	}

	/**
	 * SHOW THE SPEED
	 * @return the velocidad
	 */
	public Label getVelocidad() {
		return salidaVelocidad;
	}

	/**
	 * CHANGE THE SPEED
	 * @param velocidad the velocidad to set
	 */
	public void setVelocidad(Label velocidad) {
		this.salidaVelocidad = velocidad;
	}

	/**
	 * SHOW THE NAME
	 * @return the nombre
	 */
	public Label getNombre() {
		return nombre;
	}

	/**
	 * CHANGE THE TEXT OF TIME
	 * @return the tiempo
	 */
	public ProgressBar getTiempo() {
		return salidaTiempo;
	}

	/**
	 * SHOW THE TIME
	 * @return the tiempoInt
	 */
	public double getTiempoInt() {
		return tiempoDouble;
	}

	/**
	 * CHANGE THE TIME
	 * @param tiempoInt the tiempoInt to set
	 */
	public void setTiempoInt(double tiempoInt) {
		this.tiempoDouble = tiempoInt;
	}

	/**
	 * CHANGE POINTS
	 * @param puntosInt the puntosInt to set
	 */
	public void setPuntosInt(int puntosInt) {
		this.pointInt = puntosInt;
	}
}
