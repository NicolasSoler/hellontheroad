package InteractionsGame;

import java.util.ArrayList;
import Characters.*;
import Interface.Options;
import javafx.scene.image.Image;

public class Highway {

	//INCIALIZATION OF LISTS AND OBJECTS
	private Options configuration;
	private Protagonist player;
	private ArrayList<HighwayBackground> ListBackground;
	private ArrayList<Characters>ListCharacters;
	
	//DATA INALIZATION
	private int enemyAmount;
	private boolean bonus;
	private boolean inBonus;
	private int velocidad;
	private boolean activate;

	/**
	 * MAIN INTERFACE WHERE THE GAME AND ITS RESPECTIVE INTERACTIONS ARE TO BE CARRIED OUT
	 */
	public Highway(Options option) {
		bonus=false;
		configuration = option;
		activate=true;
		velocidad=configuration.getEnemyDifficulty();
		player = new Protagonist();
		enemyAmount=6;
		ListBackground = new ArrayList<HighwayBackground>();
		ListCharacters = new ArrayList<Characters>();
		ListBackground.add(new HighwayBackground (new Image("images/carretera.png")));
		ListBackground.add(new HighwayBackground (new Image("images/carretera.png"),-850));
		ListBackground.add(new HighwayBackground (new Image("images/espacio.png")));
		ListBackground.add(new HighwayBackground (new Image("images/espacio.png"),-850));
	}
	
	/**
	 * CHANGES THE DIFFICULTY OF THE GAME
	 */
	public void climbDifficulty() {
		velocidad+=2;
		
	}
	
	/**
	 * MOVE ALL THE LESS CHARACTERS TO THE PLAYER
	 */
	public void moveCharacters() {
		for (int i = 0; i < ListCharacters.size(); i++) {
			Characters characterTemp = ListCharacters.get(i);
			characterTemp.moveDown();
		}
	}
	/**
	 * MAINTAINS THE AMOUNT OF ENEMIES IN AN ALLOCATED PREDETERMINATE
	 */
	public void createEnemies() {
		int temporal = Math.abs(ListCharacters.size()-enemyAmount);
		for (int i = 0; i < temporal; i++) {
			int opcion = (int)(Math.random()*19)+1;
			
			if(opcion>=0&&opcion<=4) {
				DifficultEnemy difEnemy1 = new DifficultEnemy();
				difEnemy1.setSpeed(velocidad);
				ListCharacters.add(difEnemy1);
			}else if(opcion>=5&&opcion<=10) {
				MiddleEnemy midEnemy1 = new MiddleEnemy();
				midEnemy1.setSpeed(velocidad);
				ListCharacters.add(midEnemy1);
			}else if(opcion>=11&&opcion<=13) {
				Monster monsterTemp = new Monster();
				monsterTemp.setSpeed(velocidad);
				ListCharacters.add(monsterTemp);
			}else if(opcion>=14&&opcion<=16) {
				Bonus bonusTemp = new Bonus();
				bonusTemp.setSpeed(velocidad);
				ListCharacters.add(bonusTemp);
			}else {
				if(bonus&&activate) {
					ChangeBonus easyEnemy = new ChangeBonus();
					easyEnemy.setSpeed(velocidad);
					ListCharacters.add(easyEnemy);
				}else {
					EasyEnemy easyEnemy = new EasyEnemy();
					easyEnemy.setSpeed(velocidad);
					ListCharacters.add(easyEnemy);
				}
				
			}
			for (int j = 0; j < ListCharacters.size()-1; j++) {
				if(collision(ListCharacters.get(j), ListCharacters.get(ListCharacters.size()-1))) {
					ListCharacters.remove(ListCharacters.size()-1);
				}
			}
		}
	}
	
	/**
	 * ARRIVING AT A CERTAIN POINT ELIMINATES THE ENEMIES
	 */
	public void deleteEnemy() {
		for (int i = 1; i < ListCharacters.size(); i++) {
			Characters enemyTemp = ListCharacters.get(i);
			if(enemyTemp.getPosY()>=860) {
				if(!bonus) {
					if(enemyTemp.isLife()) {
						player.setScore(player.getScore()+10);
					}else {
						player.setScore(player.getScore()+20);
					}
				}else {
					if(enemyTemp.isLife()) {
						player.setScore(player.getScore()+20);
					}else {
						player.setScore(player.getScore()+40);
					}
				}
				ListCharacters.remove(enemyTemp);
				enemyTemp=null;
			}
		}
	}
	
	/**
	 * VERIFY THAT THE ENEMIES 'POSITION DO NOT PASS
	 */
	public void espaceEnemy(Characters temporal1) {
		for (int i = 1; i < ListCharacters.size(); i++) {
			Characters temporal2 = ListCharacters.get(i);
			if(collision(temporal2, temporal1)) {
				ListCharacters.remove(temporal2);
			}
		}
	}
	
	/**
	 * CALCULATE THE SHOCK WITH ENEMIES
	 */
	public void collisionEnemigos() {
		for (int i = 1; i < ListCharacters.size(); i++) {
			for (int j = 1; j < ListCharacters.size(); j++) {
				if(collision(ListCharacters.get(i), ListCharacters.get(j))) {
					int auxL=3;
					if(ListCharacters.get(i).getPosX()>ListCharacters.get(j).getPosX()) {
	        			while(auxL!=0) {
	        				ListCharacters.get(i).moveRight();
	        				ListCharacters.get(j).moveLeft();
	            			auxL=auxL-1;
	            		}
	        		}else {
	        			while(auxL!=0) {
	        				ListCharacters.get(i).moveLeft();
	        				ListCharacters.get(j).moveRight();
	            			auxL=auxL-1;
	            		}
	        		}
				}
			}
		}
	}
	
	/**
	 * SHOW THE BACKGROUND IMAGE IN A SPECIFIC POSITION
	 * @param A SPECIFIC POSITION
	 * @return THE BACKGROUND IMAGE
	 */
	public Image background(int cur) {
		
		return ListBackground.get(cur).getBackground();
	}
	
	/**
	 * MOVE ALL THE FUND
	 */
	public void moveBackground(int primero, int segundo) {
		ListBackground.get(primero).moveDown();
		ListBackground.get(segundo).moveDown();
	}
	
	/**
	 * SHOW THE GAME PLAYER
	 * @return THE PLAYER
	 */
	public Protagonist getPlayer() {
		return player;
	}
	
	
	/**
	 * SHOW IF YOU ARE IN BONUS
	 * @param inBonus the inBonus to set
	 */
	public void setInBonus(boolean inBonus) {
		this.inBonus = inBonus;
	}

	/**
	 * CHANGE IF YOU ARE IN BONUS
	 * @return the inBonus
	 */
	public boolean isInBonus() {
		return inBonus;
	}

	/**
	 * METHOD WITHOUT RETURN IN CHARGE OF ALLOWING ACCESS TO THE LIST OF IMAGES
	 * @param THE LIST OF IMAGES
	 */
	public ArrayList<HighwayBackground> getListImage() {
		return ListBackground;
	}
	
	/**
	 * SHOW THE POSITION IN X (DOUBLE)
	 * @return POSITION IN X (DOUBLE)
	 */
	public double getPosX(int cur) {
		return ListBackground.get(cur).getPosX();
	}
	
	/**
	 *  SHOW THE POSITION IN Y (DOUBLE)
	 * @return POSITION IN Y (DOUBLE)
	 */
	public double getPosY(int cur) {
		return ListBackground.get(cur).getPosY();
	}

	/**
	 * SHOW THE LIST OF ENEMIES
	 * @return THE LIST OF ENEMIES
	 */
	public ArrayList<Characters> getListCharacters() {
		return ListCharacters;
	}
	
	 /**
	  * CHANGE IF YOU ARE IN BONUS
	 * @return the bonus
	 */
	public boolean isBonus() {
		return bonus;
	}

	/**
	 * SHOW THE BONUS
	 * @param bonus the bonus to set
	 */
	public void setBonus(boolean bonus) {
		this.bonus = bonus;
	}
	

	/**
	 * SHOW THE SPEED
	 * @return the velocidad
	 */
	public int getVelocidad() {
		return velocidad;
	}

	/**
	   * VERIFYING THE COLLISION WITH THE PLAYER (AUXILIARY)
	   * @param OBJECT WITH WHICH YOU CAN COLLISION
	   * @return BOOLEAN REPRESENTING THE COLLISION
	   */
	  private boolean collisionPlayer(Characters thing2 ) {
			
			double x = player.getPosX();
			double y = player.getPosY();
			double Mx = player.getMaxX();
			double My = player.getMaxY();
			double m = thing2.getPosX();
			double n = thing2.getPosY();
			double Mm = thing2.getMaxX();
			double Mn = thing2.getMaxY();
			
			
		if((m<=x)&&(x<=Mm)&&(y>=n)&&(y<=Mn)) {
			return true;
		}else if((x<=m)&&(m<=Mx)&&(y>=n)&&(y<=Mn)) {
			return true;
		}else if((m<=x)&&(x<=Mm)&&(y<=n)&&(n<=My)) {
			return true;
		}else if((x<=m)&&(m<=Mx)&&(y<=n)&&(n<=My)) {
			return true;
		}
		
		return false;
		}
	/**
	 * DEFINE THE COLLISION BETWEEN TWO OBJECTS
	 * @param FIRST OBJET
	 * @param SECOND OBJECT
	 * @return BOOLEAN REPRESENTING THE COLLISION
	 */
	public boolean collision(Characters thing1,Characters thing2 ) {
		
		double x = thing1.getPosX();
		double y = thing1.getPosY();
		double Mx = thing1.getMaxX();
		double My = thing1.getMaxY();
		double m = thing2.getPosX();
		double n = thing2.getPosY();
		double Mm = thing2.getMaxX();
		double Mn = thing2.getMaxY();
		 		
		if((m<=x)&&(x<=Mm)&&(y>=n)&&(y<=Mn)) {
			return true;
		}else if((x<=m)&&(m<=Mx)&&(y>=n)&&(y<=Mn)) {
			return true;
		}else if((m<=x)&&(x<=Mm)&&(y<=n)&&(n<=My)) {
			return true;
		}else if((x<=m)&&(m<=Mx)&&(y<=n)&&(n<=My)) {
			return true;
		}
	return false;
	}
	
	/**
	 * SHOW THE COLLISION WITH THE PLAYERS
	 * @return
	 */
	public Characters collisionPlayer() { 
		for (int i = 0; i < ListCharacters.size(); i++) {
			if(collisionPlayer(ListCharacters.get(i))) {
				return ListCharacters.get(i);
			}
		}
		return null;
	 }
  
  
  /**
   * SHOW IF THE GAME IS ACTIVE
	 * @return the activate
	 */
	public boolean isActivate() {
		return activate;
	}

	/**
	 * CHANGE IF THE GAME IS ACTIVE
	 * @param activate the activate to set
	 */
	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	/**
	 * CHANGE THE AMOUNT OF ENEMIES
	 * @param enemyAmount
	 */
	public void setEnemyAmount(int enemyAmount) {
		this.enemyAmount = enemyAmount;
	}
}
