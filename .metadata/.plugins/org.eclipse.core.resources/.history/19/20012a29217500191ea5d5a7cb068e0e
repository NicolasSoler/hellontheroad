package InteractionsGame;

import java.util.ArrayList;
import Characters.*;
import Interface.Options;
import javafx.scene.image.Image;

public class Highway {

	private Options configuration;
	private Protagonist player;
	private ArrayList<HighwayBackground> ListBackground;
	private ArrayList<Characters>ListCharacters;
	private int enemyAmount;
	private boolean bonusLevel;

	/**
	 * MAIN INTERFACE WHERE THE GAME AND ITS RESPECTIVE INTERACTIONS ARE TO BE CARRIED OUT
	 */
	public Highway(Options option) {
		configuration = option;
		bonusLevel=false;
		player = new Protagonist();
		enemyAmount=6;
		ListBackground = new ArrayList<HighwayBackground>();
		ListCharacters = new ArrayList<Characters>();
		ListBackground.add(new HighwayBackground (new Image("images/carretera.png")));
		ListBackground.add(new HighwayBackground (new Image("images/carretera.png"),-850));
		ListBackground.add(new HighwayBackground (new Image("images/espacio.png")));
		ListBackground.add(new HighwayBackground (new Image("images/espacio.png"),-850));
		ListBackground.add(new HighwayBackground (new Image("images/cambioCaminoEspacio.png")));
		ListBackground.add(new HighwayBackground (new Image("images/cambioEspacioCamino.png"),-850));
	}
	
	/**
	 * CHANGES THE DIFFICULTY OF THE GAME
	 */
	public void climbDifficulty() {
		for (int i = 0; i < ListCharacters.size(); i++) {
			Characters characterTemp = ListCharacters.get(i);
			characterTemp.setSpeed(characterTemp.getSpeed()+1);
		}
	}
	
	/**
	 * MOVE ALL THE LESS CHARACTERS TO THE PLAYER
	 */
	public void moveCharacters() {
		for (int i = 0; i < ListCharacters.size(); i++) {
			Characters characterTemp = ListCharacters.get(i);
			characterTemp.moveDown();
		}
	}
	/**
	 * MAINTAINS THE AMOUNT OF ENEMIES IN AN ALLOCATED PREDETERMINATE
	 */
	public void createEnemies(int speed) {
		
		int temporal = Math.abs(ListCharacters.size()-enemyAmount);
		for (int i = 0; i < temporal; i++) {
			int opcion = (int)(Math.random()*19)+1;
			
			if(opcion>=0&&opcion<=4) {
				DifficultEnemy difEnemy1 = new DifficultEnemy();
				difEnemy1.setSpeed(configuration.getEnemyDifficulty()+speed);
				ListCharacters.add(difEnemy1);
			}else if(opcion>=5&&opcion<=10) {
				MiddleEnemy midEnemy1 = new MiddleEnemy();
				midEnemy1.setSpeed(configuration.getEnemyDifficulty()+speed);
				ListCharacters.add(midEnemy1);
			}else if(opcion>=11&&opcion<=13) {
				Monster monsterTemp = new Monster();
				monsterTemp.setSpeed(configuration.getEnemyDifficulty()+speed);
				ListCharacters.add(monsterTemp);
			}else if(opcion>=14&&opcion<=13) {
				Bonus bonusTemp = new Bonus();
				bonusTemp.setSpeed(configuration.getEnemyDifficulty()+speed);
				ListCharacters.add(bonusTemp);
			}else {
				if(bonusLevel) {
					ChangeLevel bonusChange = new ChangeLevel();
					bonusChange.setSpeed(configuration.getEnemyDifficulty()+speed);
					ListCharacters.add(bonusChange);
				}else {
					EasyEnemy easyEnemy = new EasyEnemy();
					easyEnemy.setSpeed(configuration.getEnemyDifficulty()+speed);
					ListCharacters.add(easyEnemy);
				}
			}
			for (int j = 0; j < ListCharacters.size()-1; j++) {
				if(collision(ListCharacters.get(j), ListCharacters.get(ListCharacters.size()-1))) {
					ListCharacters.remove(ListCharacters.size()-1);
				}
			}
		}
	}
	
	/**
	 * ARRIVING AT A CERTAIN POINT ELIMINATES THE ENEMIES
	 */
	public void deleteEnemy() {
		for (int i = 1; i < ListCharacters.size(); i++) {
			Characters enemyTemp = ListCharacters.get(i);
			if(enemyTemp.getPosY()>=860) {
				ListCharacters.remove(enemyTemp);
				enemyTemp=null;
				player.setScore(player.getScore()+10);
			}
		}
	}
	
	public void deathEnemy() {
		for (int i = 0; i < ListCharacters.size(); i++) {
			if(ListCharacters.get(i).getPosX()<=100
					||ListCharacters.get(i).getPosX()>=600) {
				ListCharacters.get(i).setLive(false);
				ListCharacters.get(i).setSkin(new Image("images/explosion.png",100,100,true,false));
			}
		}
	}
	
	/**
	 * VERIFY THAT THE ENEMIES 'POSITION DO NOT PASS
	 */
	public void espaceEnemy(Characters temporal1) {
		for (int i = 1; i < ListCharacters.size(); i++) {
			Characters temporal2 = ListCharacters.get(i);
			if(collision(temporal2, temporal1)) {
				ListCharacters.remove(temporal2);
			}
		}
	}
	
	/**
	 * SHOW THE BACKGROUND IMAGE IN A SPECIFIC POSITION
	 * @param A SPECIFIC POSITION
	 * @return THE BACKGROUND IMAGE
	 */
	public Image background(int cur) {
		
		return ListBackground.get(cur).getBackground();
	}
	
	/**
	 * MOVE ALL THE FUND
	 */
	public void moveBackground(int primero, int segundo) {
		ListBackground.get(primero).moveDown();
		ListBackground.get(segundo).moveDown();
	}
	
	/**
	 * SHOW THE GAME PLAYER
	 * @return THE PLAYER
	 */
	public Protagonist getPlayer() {
		return player;
	}
	
	/**
	 * METHOD WITHOUT RETURN IN CHARGE OF ALLOWING ACCESS TO THE LIST OF IMAGES
	 * @param THE LIST OF IMAGES
	 */
	public ArrayList<HighwayBackground> getListImage() {
		return ListBackground;
	}
	
	/**
	 * SHOW THE POSITION IN X (DOUBLE)
	 * @return POSITION IN X (DOUBLE)
	 */
	public double getPosX(int cur) {
		return ListBackground.get(cur).getPosX();
	}
	
	/**
	 *  SHOW THE POSITION IN Y (DOUBLE)
	 * @return POSITION IN Y (DOUBLE)
	 */
	public double getPosY(int cur) {
		return ListBackground.get(cur).getPosY();
	}

	/**
	 * SHOW THE LIST OF ENEMIES
	 * @return THE LIST OF ENEMIES
	 */
	public ArrayList<Characters> getListCharacters() {
		return ListCharacters;
	}
	
	 /**
	   * VERIFYING THE COLLISION WITH THE PLAYER (AUXILIARY)
	   * @param OBJECT WITH WHICH YOU CAN COLLISION
	   * @return BOOLEAN REPRESENTING THE COLLISION
	   */
	  private boolean collisionPlayer(Characters thing2 ) {
			
			double x = player.getPosX();
			double y = player.getPosY();
			double Mx = player.getMaxX();
			double My = player.getMaxY();
			double m = thing2.getPosX();
			double n = thing2.getPosY();
			double Mm = thing2.getMaxX();
			double Mn = thing2.getMaxY();
			 		
		 	if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
				return true;
			}
			if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
				return true;
			}
			if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
				return true;
			}
		return false;
		}
	/**
	 * DEFINE THE COLLISION BETWEEN TWO OBJECTS
	 * @param FIRST OBJET
	 * @param SECOND OBJECT
	 * @return BOOLEAN REPRESENTING THE COLLISION
	 */
	public boolean collision(Characters thing1,Characters thing2 ) {
		
		double x = thing1.getPosX();
		double y = thing1.getPosY();
		double Mx = thing1.getMaxX();
		double My = thing1.getMaxY();
		double m = thing2.getPosX();
		double n = thing2.getPosY();
		double Mm = thing2.getMaxX();
		double Mn = thing2.getMaxY();
		 		
	 	if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
			return true;
		}
		if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
			return true;
		}
	return false;
	}
	
	
	/**
	 * METHOD THAT VERIFIES AND CHANGES THE POSITION OF THE ENEMY
	 */
  public void collisionEnemy() {
		for (int i = 1; i < ListCharacters.size(); i++) {
			for (int j = 0; j < ListCharacters.size(); j++) {
				if(collision(ListCharacters.get(i), ListCharacters.get(j))) {
					if(ListCharacters.get(i).getPosX()>ListCharacters.get(j).getPosX()) {
						ListCharacters.get(i).moveRight();
						ListCharacters.get(j).moveLeft();
					}else {
						ListCharacters.get(j).moveRight();
						ListCharacters.get(i).moveLeft();
					}
					
				}
			}
		}
  }
	
  /**
   * VERIFYING THE COLLISION WITH THE PLAYER
   * @return Characters REPRESENTING THE COLLISION
   */
  public Characters collisionPlayer() {
	  for (int i = 0; i < ListCharacters.size(); i++) {
		  if(collisionPlayer(ListCharacters.get(i))) {
			  return ListCharacters.get(i);
		  }
	  }
	  return null;
  }
  
  public void setEnemyAmount(int enemyAmount) {
		this.enemyAmount = enemyAmount;
	}

public boolean isBonusLevel() {
	return bonusLevel;
}

public void setBonusLevel(boolean bonusLevel) {
	this.bonusLevel = bonusLevel;
}
  
}
