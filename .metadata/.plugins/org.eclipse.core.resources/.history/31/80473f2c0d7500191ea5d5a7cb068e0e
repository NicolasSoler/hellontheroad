package InteractionsGame;

import java.util.Timer;
import java.util.TimerTask;

import Characters.Bonus;
import Characters.Characters;
import Characters.Monster;
import Interface.Options;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class StartGame extends Scene implements EventHandler<KeyEvent>{

	private Canvas canvasLevel;
	private GraphicsContext context;
	private Highway movingRoads;
	private int background;
	private int backgroundPluss;
	private boolean finale;
	private Score puntaje;
	private Button returnW;
	private Group principal;
	private String level;
	private boolean moveEnemy;
	private int graphicsPlay;
	private int advance;
	private int speed;
	/**
	 * CONSTRUCTOR OF THE STARTGAME CLASS WITH ITS PREDETERMINED VALUES
	 * @param ROOT IN WHICH THE DATA WILL BE STORED
	 * @param WIDTH THAT IS GOING TO HAVE THE SCREEN
	 * @param HEIGHT THAT WILL HAVE THE SCREEN
	 */
	public StartGame(Group root, double width, double height,Options option) {
		super(root);
		graphicsPlay=0;
		canvasLevel= new Canvas(width,height);
		movingRoads = new Highway(option);
		level = option.getEntradaContraseña().getText();
		finale = false;
    	returnW=new Button("Volver al menu");
    	returnW.setMinSize(100, 100);
    	returnW.setDefaultButton(false);
		returnW.setStyle("-fx-base: rgb(153,0,23);");
		puntaje = new Score();
		speed=0;
		movingRoads.createEnemies(speed);
		moveEnemy=false;
		selecLevel();
		principal=root;
		root=null;
		puntaje.setVelocidadInt(option.getEnemyDifficulty());
		principal.getChildren().addAll(canvasLevel,getPuntaje().getVentanaGeneral(),returnW);
		setContext(canvasLevel.getGraphicsContext2D());
		this.setOnKeyPressed(this);
	}
	
	public void changeDif() {
		if(advance<1000) {
			advance=advance+1;
		}else {
			advance=0;
			if(graphicsPlay==0) {
				graphicsPlay=1;
				speed=speed+1;
				puntaje.setVelocidadInt(puntaje.getVelocidadInt()+1);
			}else {
				graphicsPlay=0;
				puntaje.setVelocidadInt(puntaje.getVelocidadInt()+1);
			}
		}
	}
	
	public void selecLevel() {
		
		if(graphicsPlay==1) {
			background=2;
			backgroundPluss=3;
			for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
				movingRoads.getListCharacters().get(i).transformShip();
			}
			movingRoads.getPlayer().transformShip();
		}else {
			background=0;
			backgroundPluss=1;
			for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
				movingRoads.getListCharacters().get(i).transformCar();
			}
			movingRoads.getPlayer().transformCar();
		}
		
	}
	
	/**
	 * SHOW THE FINAL SCREEN
	 */
	public void showEnd() {
		principal.getChildren().clear();
		principal.getChildren().addAll(puntaje.getPantallaFinal().getVentanaJuego());
	}
	
	/**
	 * 
	 */
	public void chanceScene() {
		if(graphicsPlay==1) {
			level="PrDurlAO";
			puntaje.getPantallaFinal().getSalidaContraseña().setText(level);
			background=2;
			backgroundPluss=3;
		}else {
			level="GHoOps";
			puntaje.getPantallaFinal().getSalidaContraseña().setText(level);
			background=0;
			backgroundPluss=1;
		}
	}
	
	/** 
	 * METHOD IN CHARGE OF RUNNING THE CHARACTERES
	 */
	public void runCharacter() {
		new AnimationTimer() {
			@Override
			public void handle(long now) {
				
				if(!finale) {
					//SHOW THE ENEMIES CREATED IN THE GAME
					for (int i = 0; i < movingRoads.getListCharacters().size(); i++) {
						context.drawImage(movingRoads.getListCharacters().get(i).getSkin(),
								movingRoads.getListCharacters().get(i).getPosX(), 
								movingRoads.getListCharacters().get(i).getPosY());
					}
					
					
					//MOVES AND INTERCALLS THE IMAGES OF THE FUND ACCORDING TO ITS POSITION
					if(movingRoads.getListImage().get(background).getPosY()>=850) {
						movingRoads.getListImage().get(background).setPosY(-850);
						if(background==4) {
							background=2;
						}
					}
					if(movingRoads.getListImage().get(backgroundPluss).getPosY()>=850) {
						movingRoads.getListImage().get(backgroundPluss).setPosY(-850);
						if(backgroundPluss==5) {
							backgroundPluss=3;
						}
					}
					
					//MOVEMENT LIMITER X AXIS AND ALSO CONSEQUENCE OF COLLISION
					if((movingRoads.getPlayer().getPosX()<=100&&movingRoads.getPlayer().getPosY()>=0.0
							&&movingRoads.getPlayer().getPosY()<=780.0
							||movingRoads.getPlayer().getPosX()>=600&&movingRoads.getPlayer().getPosY()>=0.0
							&&movingRoads.getPlayer().getPosY()<=780.0)) {
						
						movingRoads.getPlayer().setAction(false);
						movingRoads.getPlayer().dead();
					}
					
					if(movingRoads.collisionPlayer()!=null) {
						collision(movingRoads.collisionPlayer());
					}
					
					//SHOW THE PLAYER
					context.drawImage(movingRoads.getPlayer().getSkin(),
										movingRoads.getPlayer().getPosX(), 
										movingRoads.getPlayer().getPosY());
					changeDif();
					
				}else {
					stop();
				}
			}
		}.start();
	}
	
	/**
	 * METHOD IN CHARGE OF RUNNING THE GAME
	 */
	public void runLevel() {
		new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				if(!finale){
					if(movingRoads.getPlayer().getFury()<0) {
						movingRoads.getPlayer().setAction(false);
					}
					
					movingRoads.collisionEnemy();
					puntaje.actualizarDatos();
					//SHOWS THE FIRST IMAGE OF THE FUND
					context.drawImage(movingRoads.background(background),
										movingRoads.getPosX(background),
										movingRoads.getPosY(background));
					
					//SHOW THE SECOND IMAGE OF THE FUND
					context.drawImage(movingRoads.background(backgroundPluss), 
										movingRoads.getPosX(backgroundPluss),
										movingRoads.getPosY(backgroundPluss));
					

					//MANAGE THE MOVEMENT DEPENDING ON IF THE PLAYER IS "LIVE"
					if(movingRoads.getPlayer().isAction()) {
						
						new Timer().schedule(new TimerTask() {
					        @Override
					        public void run() {
					        	if(movingRoads.getListImage().get(background).getSpeed()<=20) {
					        		movingRoads.getListImage().get(background).setSpeed(movingRoads.getListImage().get(background).getSpeed()+1);
					        		movingRoads.getListImage().get(backgroundPluss).setSpeed(movingRoads.getListImage().get(backgroundPluss).getSpeed()+1);
					        		moveEnemy=true;
					        	}
					        }
					    }, 1000);
						
						if(moveEnemy) {
							movingRoads.moveCharacters();
						}
						
						addEnemy(movingRoads.getListCharacters().size());
						selecLevel();
						chanceScene();
						movingRoads.createEnemies(speed);
						movingRoads.deleteEnemy();
						movingRoads.moveBackground(background,backgroundPluss);
						movingRoads.getPlayer().loseFury();
						puntaje.setTiempoInt(movingRoads.getPlayer().getFury());
						puntaje.setPuntosInt(movingRoads.getPlayer().getScore());
						puntaje.getPantallaFinal().getSalidaPuntos().setText(""+movingRoads.getPlayer().getScore());
			        	
					}
					
					//MANAGE THE END OF THE GAME SCREEN
					if(!movingRoads.getPlayer().isAction()) {
						new Timer().schedule(new TimerTask() {
					        @Override
					        public void run() {
					        	finale=true;
					        }
					    }, 1000);
					}	
				}else {
					stop();
					showEnd();
				}
			}
		}.start();
	}
	
	public void addEnemy(int cant) {
		if(cant<5) {
			movingRoads.setEnemyAmount(cant+1);
		}
	}
	
	@Override
	public void handle(KeyEvent event) {
		
		if(moveEnemy) {
			if(event.getCode().equals(KeyCode.W)||event.getCode().equals(KeyCode.UP)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(movingRoads.getPlayer().getPosY()>=360) {
					movingRoads.getPlayer().moveUp();
				}
			}else if(event.getCode().equals(KeyCode.D)||event.getCode().equals(KeyCode.RIGHT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				movingRoads.getPlayer().moveRight();
			}else if(event.getCode().equals(KeyCode.A)||event.getCode().equals(KeyCode.LEFT)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				movingRoads.getPlayer().moveLeft();
			}else if(event.getCode().equals(KeyCode.S)||event.getCode().equals(KeyCode.DOWN)&&event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				if(movingRoads.getPlayer().getPosY()<=750) {
					movingRoads.getPlayer().moveDown();
				}
			}
		}
	}
	
	private void collision(Characters objeto) {
		if(objeto instanceof Bonus) {
			movingRoads.getPlayer().setScore(movingRoads.getPlayer().getScore()+50);
			movingRoads.getPlayer().setFury(movingRoads.getPlayer().getFury()+(movingRoads.getPlayer().getFury()/2));
		}else if(objeto instanceof Monster) {
			movingRoads.getPlayer().setAction(false);
			movingRoads.getPlayer().dead();
		}else {
			moveEnemy=false;
			movingRoads.getPlayer().setFury(movingRoads.getPlayer().getFury()-5);
			int auxL = 3;
        		if(objeto.getPosX()<movingRoads.getPlayer().getPosX()) {
        			while(auxL!=0) {
        				objeto.moveLeft();
            			movingRoads.getPlayer().moveRight();
            			auxL=auxL-1;
            		}
        		}else {
        			while(auxL!=0) {
        				objeto.moveRight();
            			movingRoads.getPlayer().moveLeft();
            			auxL=auxL-1;
            		}
        		}
        		
        		new Timer().schedule(new TimerTask() {
			        @Override
			        public void run() {
			        	moveEnemy=true;
			        }
			    }, 100);
			
		}
		
	}
	
	/**
	 * CHANGES THE GRAPHIC CONTEXT THAT SHOWS ON SCREEN
	 * @param GRAPHICAL CONTEXT OF THE CAVAS
	 */
	public void setContext(GraphicsContext context) {
		this.context = context;
	}
	
	/**
	 * @return the finale
	 */
	public boolean isFinale() {
		return finale;
	}

	/**
	 * @param finale the finale to set
	 */
	public void setFinale(boolean finale) {
		this.finale = finale;
	}

	/**
	 * @return the canvasLevel
	 */
	public Canvas getCanvasLevel() {
		return canvasLevel;
	}

	/**
	 * @param canvasLevel the canvasLevel to set
	 */
	public void setCanvasLevel(Canvas canvasLevel) {
		this.canvasLevel = canvasLevel;
	}

	/**
	 * @return the movingRoads
	 */
	public Highway getMovingRoads() {
		return movingRoads;
	}

	/**
	 * @param movingRoads the movingRoads to set
	 */
	public void setMovingRoads(Highway movingRoads) {
		this.movingRoads = movingRoads;
	}

	/**
	 * @return the background
	 */
	public int getBackground() {
		return background;
	}

	/**
	 * @param background the background to set
	 */
	public void setBackground(int background) {
		this.background = background;
	}

	/**
	 * @return the backgroundPluss
	 */
	public int getBackgroundPluss() {
		return backgroundPluss;
	}

	/**
	 * @param backgroundPluss the backgroundPluss to set
	 */
	public void setBackgroundPluss(int backgroundPluss) {
		this.backgroundPluss = backgroundPluss;
	}
	/**
	 * @return the puntaje
	 */
	public Score getPuntaje() {
		return puntaje;
	}

	/**
	 * @param puntaje the puntaje to set
	 */
	public void setPuntaje(Score puntaje) {
		this.puntaje = puntaje;
	}

	/**
	 * @return the context
	 */
	public GraphicsContext getContext() {
		return context;
	}

	public Button getReturnW() {
		return returnW;
	}

	public void setReturnW(Button returnW) {
		this.returnW = returnW;
	}
	
}
